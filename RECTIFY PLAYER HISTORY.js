// ====================================================================================================
//
// Cloud Code for InvalidateQuestion, write your code here to customise the GameSparks platform.
//
// For details of the GameSparks Cloud Code API see https://portal.gamesparks.net/docs.htm			
//
// ====================================================================================================

// This script is trusthworthy.

var questionid = Spark.getData().questionid

var scoreValueByTheme = [120,150,150,80,60,100,60,60,120];
var today = new Date();
today.setTime(today.getTime() - 3*60*60*1000)
var todayDay = today.getDate();
var todayMonth = today.getMonth(); // No +1 on the month because we'll create a new date
var todayYear = today.getFullYear();

today = new Date(todayYear, todayMonth, todayDay) // To make the clock irrelevant

var questions_collection = Spark.runtimeCollection("question");
var answers_collection = Spark.runtimeCollection("playerAnswers");

var invalid_question = questions_collection.findOne({"_id": {"$oid": questionid}});
var question_theme = parseInt(invalid_question.theme.charAt(0)) - 1;
var question_value = scoreValueByTheme[question_theme];
question_date = invalid_question.date

var question_day = question_date.charAt(0) + question_date.charAt(1);
var question_month = question_date.charAt(3) + question_date.charAt(4);
var question_year = question_date.charAt(6) + question_date.charAt(7)
                    + question_date.charAt(8) + question_date.charAt(9);

var question_dateobject = new Date(parseInt(question_year), parseInt(question_month)-1,
                                    parseInt(question_day));

if (question_dateobject >= today){
    Spark.setScriptError("invalidate_error", "Você só pode anular uma questão que já passou. Aguarde"+ 
                                            " virar o dia ou, se a pergunta ainda não está ao vivo,"+ 
                                            " considere alterá-la/removê-la.");
    Spark.exit()
} else{
    if (invalid_question.invalidated == null || invalid_question.invalidated == false){
      Spark.setScriptError("invalidate_error", "Esta questão não foi anulada.");
      Spark.exit()
    } else{
        // Get the PlayerHistory
        all_answers = answers_collection.find({});
        while (all_answers.hasNext()){
            player_answer = all_answers.next();
            player_id = player_answer.playerId;
            answers_array = player_answer.answers;
            
            will_give_free_question = true;
            user_answered_invalidated_question = false;
            if (answers_array != null){
                for (i = 0 ; i < answers_array.length ; i++){
                    current_question = answers_array[i];
                    if (current_question.questionId == questionid){
                        user_answered_invalidated_question = true;
                        
                        answers_collection.update({"playerId": player_id, "answers.questionId" : questionid},
                        {
                          "$set":
                          {
                              "answers.$.isCorrect" : true,
                              "answers.$.wasInvalidated" : true
                          }
                        });
                        break;
                    }
                }
            }
        }
    }
}