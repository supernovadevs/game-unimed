﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Utilities;

public class AchievementsManager : MonoBehaviour {

  public static AchievementsManager instance;

  public RectTransform achievementPopUp;
  public Text achievementPopUpText;
  public List<string> achievementStringsToPop;

  public bool isShowingAchievement = false;

  void Awake(){
    instance = this;
  }

  public static AchievementsManager GetInstance(){
    return instance;
  }



  public bool IsAchievementUnlocked(int achievementId){
    return IsAchievementUnlocked(Util.GetAchievementShortCode(achievementId));
  }

  public bool IsAchievementUnlocked(string shortCode){
    if(GlobalGameData.GetInstance().achievementsList != null){
      foreach(string unlockeds in GlobalGameData.GetInstance().achievementsList){
        if(unlockeds == shortCode){
          return true;
        }
      }
    }
    return false;
  }


  public void SendGetAchievementRequest(string shortCode){
    if(AchievementsManager.GetInstance().IsAchievementUnlocked(shortCode) == true) {
      Debug.Log("Achievement " +  shortCode + " already got!");
      return;
    }

    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey("awardAchievement")
      .SetEventAttribute("ACHIEVEMENTNAME", shortCode)
      .Send((response) => {
        if(!response.HasErrors){
          Debug.LogWarning("Award Achievement Event sent!");
        }else{
          Debug.LogError("Award Achievement Event ERROR!");
        }
      });
  }

  public void SendLogEventOpenDashboard(){
    if(AchievementsManager.GetInstance().IsAchievementUnlocked("conectado") ||
       GlobalGameData.GetInstance().hasCheckedDashboard) {
      return;
    }

    GlobalGameData.GetInstance().hasCheckedDashboard = true;

    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey("userCheckedDashboard")
      .Send((response) => {
      if(!response.HasErrors){
        Debug.LogWarning("Open Dashboard Event sent!");
      }else{
        Debug.LogError("Open Dashboard Event ERROR!");
      }
    });
  }

  public void SendLogEventAnswerQuickly(){
    if(AchievementsManager.GetInstance().IsAchievementUnlocked("rapidoNoGatilho")) {
      return;
    }

    GlobalGameData.GetInstance().hasCheckedDashboard = true;

    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey("userAnsweredFast")
      .Send((response) => {
      if(!response.HasErrors){
        Debug.LogWarning("Answered Quickly Event sent!");
      }else{
        Debug.LogError("Answered Quickly Event ERROR!");
      }
    });
  }

  public void SendLogEventOpenLeaderboard(){
    if(AchievementsManager.GetInstance().IsAchievementUnlocked("competitivo") ||
       GlobalGameData.GetInstance().hasCheckedLeaderboard) {
      return;
    }

    GlobalGameData.GetInstance().hasCheckedLeaderboard = true;

    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey("userCheckedLeaderboard")
      .Send((response) => {
      if(!response.HasErrors){
        Debug.LogWarning("Open Leaderboard Event sent!");
      }else{
        Debug.LogError("Open Leaderboard Event ERROR!");
      }
    });
  }


  public void AddNewAchievement(string achievementGot){
    achievementStringsToPop.Add(achievementGot);
    CheckIfCanPopNewAchievement();
  }


  void CheckIfCanPopNewAchievement(){
    if(isShowingAchievement == false &&
      achievementStringsToPop.Count != 0) {
      PopAchievement();
    }
  }



  public void PopAchievement(){
    string textToShow = achievementStringsToPop[0];
    achievementStringsToPop.RemoveAt(0);

    SoundManager.GetInstance().PlaySfx("Achievement Unlocked");

    isShowingAchievement = true;

    achievementPopUpText.text = textToShow;
    achievementPopUp.DOPivotY(1f, 1f).OnComplete( WaitAndReturnPopUp );
  }

  void WaitAndReturnPopUp(){
    StartCoroutine( WaitAndBack() );
  }

  IEnumerator WaitAndBack(){
    yield return new WaitForSeconds(3f);
    achievementPopUp.DOPivotY(0f, 1f).OnComplete( FinishPopup );
  }

  void FinishPopup(){
    isShowingAchievement = false;
    CheckIfCanPopNewAchievement();
  }

}
