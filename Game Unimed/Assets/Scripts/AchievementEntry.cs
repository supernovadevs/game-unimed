﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Utilities;

public class AchievementEntry : MonoBehaviour {

  public Image achievementImage;
  public Text achievementNameText;
  public Text achievementDescriptionText;


  public void UpdateAchievementId(int id){
    UpdateName( Util.GetAchievementName(id) );
    UpdateDescription( Util.GetAchievementDescription(id) );
  }

  public void UpdateName(string name){
    achievementNameText.text = name;
  }

  public void UpdateDescription(string description){
    achievementDescriptionText.text = description;
  }

}
