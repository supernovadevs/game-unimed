﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ChangeBundleName : MonoBehaviour {

  public string version = "none";

  void Update () {
    if(Application.platform == RuntimePlatform.WindowsEditor ||
	     Application.platform == RuntimePlatform.OSXEditor) {
      #if UNITY_ANDROID && UNITY_EDITOR
      if(version != "android"){
        Debug.Log("RUNNING ANDROID");
        UnityEditor.PlayerSettings.bundleIdentifier = "br.com.unimedfortaleza.JornadaGame";
        version = "android";
      }
      #endif
      #if UNITY_IOS && UNITY_EDITOR
      if(version != "ios"){
        Debug.Log("RUNNING IOS");
        UnityEditor.PlayerSettings.bundleIdentifier = "br.com.unimedfortaleza.JornadaUnimed";
        version = "ios";
      }
      #endif
    }
	}
}
