﻿using UnityEngine;
using System.Collections;
using Utilities;

public class SoundManager : MonoBehaviour {

  private static SoundManager instance = null;

  public PartialLoopPlayer musicPlayer;

  public AudioClip[] songIntros;
  public AudioClip[] songLoops;

  [HideInInspector] public AudioSource[] sfxSource;
	public AudioClip[] generalSound;

	public AudioClip[] confirmSound;
	public AudioClip[] bigConfirmSound;
	public AudioClip[] cancelSound;
	public AudioClip[] cursorSound;
	public AudioClip[] forbiddenSound;

  private float lowPitchRange = 0.95f;
  private float highPitchRange = 1.05f;

  const float defaultSfxVolume = 1f;

	void Awake () {
    if(instance == null) {
      instance = this;
    } else if(instance != this) {
      Destroy(gameObject);
    }

    sfxSource = GetComponents<AudioSource>();

		DontDestroyOnLoad(gameObject);
	}

  public static SoundManager GetInstance(){
    if(instance == null) {
      return GameObject.FindWithTag("Sound Manager").GetComponent<SoundManager>();
    }
    return instance;
  }
    

  public void PlaySong(SongNames songName){
    for(int i=0;  i<songLoops.Length; i++){
      if(songLoops[i].name == songName.ToString()){
        PlaySong(i);
      }
    }
  }

  private void PlaySong(int songId){
    if(musicPlayer.idPlaying!= songId){
      musicPlayer.SetMusic(songIntros[songId], songLoops[songId], songId);
      musicPlayer.StartPlaying();
    }
  }


  public void PlayMusicForToday(){
    System.DateTime date = System.DateTime.Now;

    switch(date.DayOfWeek){
    case System.DayOfWeek.Sunday:
      PlaySong(Utilities.SongNames.song1_loop);
      break;
    case System.DayOfWeek.Monday:
      PlaySong(Utilities.SongNames.song1_loop);
      break;
    case System.DayOfWeek.Tuesday:
      PlaySong(Utilities.SongNames.song5_loop);
      break;
    case System.DayOfWeek.Wednesday:
      PlaySong(Utilities.SongNames.song3_loop);
      break;
    case System.DayOfWeek.Thursday:
      PlaySong(Utilities.SongNames.song2_loop);
      break;
    case System.DayOfWeek.Friday:
      PlaySong(Utilities.SongNames.song4_loop);
      break;
    case System.DayOfWeek.Saturday:
      PlaySong(Utilities.SongNames.song4_loop);
      break;
    }
  }



	void PlayAudioClip(AudioClip clip){		
		foreach( AudioSource audio in sfxSource ){
			if( !audio.isPlaying ){
				audio.clip = clip;
				audio.Play();
				break;
			}
		}
	}
	
	public void PlayRandomPitchAudioClip(AudioClip clip){
		float randomPitch = Random.Range(lowPitchRange, highPitchRange);
		
		foreach( AudioSource audio in sfxSource ){
			if( !audio.isPlaying ){
				audio.clip = clip;
				audio.Play();
				break;
			}
		}
	}

	public void RandomizeSfx(AudioClip[] clips){
		int randomIndex = Random.Range(0, clips.Length);
		PlayAudioClip( clips[randomIndex] );
	}
	
	public void RandomPitchedSfx(AudioClip[] clips){
		int randomIndex = Random.Range(0, clips.Length);
		PlayRandomPitchAudioClip( clips[randomIndex] );
	}

  public void PlaySfx(string sfxName){
    foreach(AudioClip ac in generalSound) {
      if(ac.name == sfxName){
        PlayAudioClip(ac);
        return;
      }
    }
	}


	

  public void PlayConfirmSound(){
    RandomizeSfx(confirmSound);
  }

  public void PlayBigConfirmSound(){
    RandomizeSfx(bigConfirmSound);
  }

  public void PlayCancelSound(){
    RandomizeSfx(cancelSound);
  }

  public void PlayCursorSound(){
    RandomizeSfx(cursorSound);
  }

  public void PlayForbiddenSound(){
    RandomizeSfx(forbiddenSound);
  }


  public void TurnVolumeOn(bool value){
    TurnSfxOn(value);
    TurnMusicOn(value);
  }

  void TurnSfxOn(bool value){
    foreach(AudioSource audio in sfxSource){
      if(value == true){
        audio.volume = defaultSfxVolume;
      }else{
        audio.volume = 0f;
      }
    }
  }

  void TurnMusicOn(bool value){
    musicPlayer.TurnMusicOn(value);
  }
}
