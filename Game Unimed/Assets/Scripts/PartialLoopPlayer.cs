﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Plays an intro clip only once, and then loops another one. /// </summary>
public class PartialLoopPlayer : MonoBehaviour {
	
	public AudioSource introSource;
	public AudioSource loopSource;
  public int idPlaying = -1;
	private bool started = false;

  const float defaultMusicVolume = 0.5f;
	
	void Start(){
		if( !started )
			StartPlaying();
	}

  #if UNITY_WEBGL
  void Update(){
//    Debug.Log("Test. Loop is " + (loopSource.loop ? "true":"false") );
    if(loopSource.clip != null && !loopSource.isPlaying && !introSource.isPlaying) {
      Debug.Log("Loop is " + (loopSource.loop ? "true":"false") );
      loopSource.Play();
    }
  }
  #endif

	
	public void SetMusic(AudioClip intro, AudioClip loop, int id){
		introSource.Stop();
		loopSource.Stop();
		
		introSource.clip = intro;
		loopSource.clip = loop;
		
//		StartPlaying();
    idPlaying = id;
	}
	
	public void StartPlaying(){
//		if(introSource.isPlaying || loopSource.isPlaying){
//			return;
//		}
//    introSource.volume = 0.5f;
//    loopSource.volume = 0.5f;

		if( introSource.clip!=null ){
      if(Application.platform == RuntimePlatform.WebGLPlayer){
        loopSource.Play();
      }else{
  			introSource.Play();
  		  loopSource.PlayDelayed(introSource.clip.length);
      }
		}else if( loopSource.clip!=null ){
			loopSource.Play();
		}
		started = true;
	}

  public void TurnMusicOn(bool value){
    if(value == true){
      introSource.volume = defaultMusicVolume;
      loopSource.volume = defaultMusicVolume;
    }else{
      introSource.volume = 0f;
      loopSource.volume = 0f;
    }
  }
	
}
