﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

namespace Utilities {

  public enum Directions {Up, Right, Down, Left};

  public enum SceneNames {Login, GameMap, PhotoLoader};

  public enum SongNames {song1_loop, song2_loop, song3_loop, song4_loop, song5_loop};

  public class Util {

    private static string[] positionNames = {"Aprendiz", "Estagiário", "Assistente", "Analista Júnior",
      "Analista Pleno", "Analista Sênior", "Coordenador", "Gerente", "Superintendente",
      "Diretor", "Presidente", "Presidente 2", "Presidente 3", "Presidente 4"
    };
    private static int[] positionCosts = {
      0,
      400,
      1000,
      1790,
      2940,
      4430,
      6240,
      8390,
      10860,
      13670,
      16800,
      24000,
      32640,
      50000
    };


    public static int GetPositionIdByPoints(int playerPoints) {
      int positionId = -1;

      for(int i = 0; i < positionCosts.Length; i++) {
        if(playerPoints >= positionCosts[i]) {
          positionId = i + 1;
        } else {
          break;
        }
      }
      return positionId;
    }

    public static int PointsToGetThisPosition(int positionId) {
      return positionCosts[positionId - 1];
    }

    public static int PointsToGetNextPosition(int positionId) {
      if(positionId != positionCosts.Length) {
        int pointsToNextLevel = PointsToGetThisPosition(positionId + 1);
        return pointsToNextLevel - GlobalGameData.GetInstance().userScore;
      }

      return -1;
    }

    public static string GetPositionNameById(int positionId) {
      return positionNames[positionId - 1];
    }


    public static int PointsToEarnByTheme(int theme) {
      switch(theme) {
        case 1:
          return 120;
        case 2:
          return 150;
        case 3:
          return 150;
        case 4:
          return 80;
        case 5:
          return 60;
        case 6:
          return 100;
        case 7:
          return 60;
        case 8:
          return 60;
        case 9:
          return 120;
      }
      return 0;
    }




    public static string GetCharacterName(int id){
      switch(id) {
        case 1:
          return "Sofia";
        case 2:
          return "Arthur";
        case 3:
          return "Roberta";
        case 4:
          return "Ana";
        case 5:
          return "Dr. José Carlos";
      }
      return "";
    }


    public static string GetAchievementShortCode(int id) {
      switch(id) {
        case 1:
          return "unimedNoPC";
        case 2:
          return "unimedNaMao";
        case 3:
          return "explorador";
        case 4:
          return "conhecidoDaSofia";
        case 5:
          return "amigoDaSofia";
        case 6:
          return "especialistaEmRH";
        case 7:
          return "conhecidoDoArthur";
        case 8:
          return "amigoDoArthur";
        case 9:
          return "especialistaEmPlanejamento";
        case 10:
          return "conhecidoDaRoberta";
        case 11:
          return "amigoDaRoberta";
        case 12:
          return "especialistaEmAtendimento";
        case 13:
          return "conhecidoDaAna";
        case 14:
          return "amigoDaAna";
        case 15:
          return "especialistaEmSaude";
        case 16:
          return "conhecidoDoDrJoseCarlos";
        case 17:
          return "amigoDoDrJoseCarlos";
        case 18:
          return "especialistaEmUnimed";
        case 19:
          return "semanaProdutiva";
        case 20:
          return "mesImbativel";
        case 21:
          return "conectado";
        case 22:
          return "competitivo";
        case 23:
          return "assiduo";
        case 24:
          return "superAssiduo";
        case 25:
          return "rapidoNoGatilho";
        case 26:
          return "top100";
        case 27:
          return "futuroPromisor";
        case 28:
          return "liderancaNata";
        case 29:
          return "unimedFortalezaNoTopo";
        case 30:
          return "umPoucoDoisBom";
        case 31:
          return "fazendoAsMalas";
        case 32:
          return "venceuNaVida";
      }
      return null;
    }

    public static string GetAchievementName(int id) {
      switch(id) {
        case 1:
          return "Unimed na Web";
        case 2:
          return "Unimed na Mão";
        case 3:
          return "Explorador";
        case 4:
          return "Conhecido da Sofia";
        case 5:
          return "Amigo da Sofia";
        case 6:
          return "Especialista em RH";
        case 7:
          return "Conhecido do Arthur";
        case 8:
          return "Amigo do Arthur";
        case 9:
          return "Especialista em Planejamento";
        case 10:
          return "Conhecido da Roberta";
        case 11:
          return "Amigo da Roberta";
        case 12:
          return "Especialista em Atendimento";
        case 13:
          return "Conhecido da Ana";
        case 14:
          return "Amigo da Ana";
        case 15:
          return "Especialista em Saúde";
        case 16:
          return "Conhecido do Dr. José Carlos";
        case 17:
          return "Amigo do Dr. José Carlos";
        case 18:
          return "Especialista em Unimed";
        case 19:
          return "Semana Produtiva";
        case 20:
          return "Mês Imbatível";
        case 21:
          return "Conectado";
        case 22:
          return "Competitivo";
        case 23:
          return "Assíduo";
        case 24:
          return "Super Assíduo";
        case 25:
          return "Rápido no Gatilho";
        case 26:
          return "Top 100";
        case 27:
          return "Futuro Promissor";
        case 28:
          return "Liderança Inspiradora";
        case 29:
          return "#UnimedFortalezaNoTopo";
        case 30:
          return "Um é bom, dois é melhor!";
        case 31:
          return "Fazendo as Malas";
        case 32:
          return "Mestre da Excelência";
      }
      return null;
    }

    public static string GetAchievementDescription(int id) {
      switch(id) {
        case 1:
          return "Fazer login no jogo usando a versão web";
        case 2:
          return "Fazer login no jogo usando o aplicativo mobile";
        case 3:
          return "Responder pelo menos uma pergunta em cada cenário";
        case 4:
          return "Acertar 5 perguntas feitas pela Sofia";
        case 5:
          return "Acertar 10 perguntas feitas pela Sofia";
        case 6:
          return "Acertar 30 perguntas feitas pela Sofia";
        case 7:
          return "Acertar 5 perguntas feitas pelo Arthur";
        case 8:
          return "Acertar 10 perguntas feitas pelo Arthur";
        case 9:
          return "Acertar 30 perguntas feitas pelo Arthur";
        case 10:
          return "Acertar 5 perguntas feitas pela Roberta";
        case 11:
          return "Acertar 10 perguntas feitas pela Roberta";
        case 12:
          return "Acertar 30 perguntas feitas pela Roberta";
        case 13:
          return "Acertar 5 perguntas feitas pela Ana";
        case 14:
          return "Acertar 10 perguntas feitas pela Ana";
        case 15:
          return "Acertar 30 perguntas feitas pela Ana";
        case 16:
          return "Acertar 5 perguntas feitas pelo Dr. José Carlos";
        case 17:
          return "Acertar 10 perguntas feitas pelo Dr. José Carlos";
        case 18:
          return "Acertar 30 perguntas feitas pelo Dr. José Carlos";
        case 19:
          return "Acertar 10 perguntas em uma semana";
        case 20:
          return "Acertar 40 perguntas em um mês";
        case 21:
          return "Acessar seu painel de informações em 10 dias diferentes";
        case 22:
          return "Acessar o placar em 10 dias diferentes";
        case 23:
          return "Responder pelo menos uma pergunta em 20 dias diferentes\ndurante um mês";
        case 24:
          return "Responder pelo menos uma pergunta em 20 dias diferentes\ndurante dois meses (consecutivos ou não)";
        case 25:
          return "Acertar 10 perguntas em menos de 1 minuto (cada)";
        case 26:
          return "Alcançar o top 100 a partir do segundo mês de jogo";
        case 27:
          return "Alcançar o cargo de Analista Júnior";
        case 28:
          return "Alcançar o cargo de Gerente";
        case 29:
          return "Alcançar o cargo de Presidente";
        case 30:
          return "Alcançar o 2º Tíquete";
        case 31:
          return "Alcançar o 3º Tíquete";
        case 32:
          return "Alcançar o 4º Tíquete";
      }
      return null;
    }

    public static string GetFormatedDisplayName(string displayName){
      if(displayName == "" || displayName == null)
        return "";

      string[] nameParts = displayName.Split(' ');
      string finalString = "";

      foreach(string part in nameParts){
        if(finalString != "") {
          finalString += " ";
        }
        if(part.Length > 0){
          finalString += part.Substring(0, 1).ToUpper() + part.Substring(1, part.Length-1).ToLower();
        }
      }

      return finalString;
    }

    public static void UnselectButton() {
      EventSystem.current.SetSelectedGameObject(null);
    }
  }
}
