﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Utilities;
using System.IO;

public class LeaderboardEntry : MonoBehaviour {

  public Text rankingText;
  public Text nameText;
  public Text positionText;
  public Text scoreText;
  public Image medalImage;

  public Color othersColor;
  public Color myColor;

  public void UpdateEntry(int rank, string displayName, int score){
    gameObject.SetActive(true);
    rankingText.text = rank.ToString();

    nameText.text = "---";
    scoreText.text = score.ToString();
    positionText.text = Util.GetPositionNameById( Util.GetPositionIdByPoints(score) );
    nameText.text = Util.GetFormatedDisplayName(displayName);
    SetColor(displayName);

//    LogEntryToFile(displayName);
  }

  void SetColor(string name){
    if(name == GlobalGameData.GetInstance().userDisplayName) {
      foreach(Image i in GetComponentsInChildren<Image>()) {
        i.color = myColor;
      }
    } else {
      foreach(Image i in GetComponentsInChildren<Image>()) {
        i.color = othersColor;
      }
    }
  }

  public void Clear(){
    gameObject.SetActive(false);
  }

  public void LogEntryToFile(string name){
    string filePath = Application.dataPath + "/list.txt";

    File.AppendAllText(filePath, name+"\n");
  }
}
