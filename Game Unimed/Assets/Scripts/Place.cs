﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Place : MonoBehaviour {

  public string defaultVsnScript;
  public int placeId;
  public GameObject[] questionIcon;
  public List<Question> questionList;
  private Tween blinkTweener = null;

  void Awake(){
    questionList = new List<Question>();
    UpdateQuestionIcons();
  }


  public void Clicked() {
    SoundManager.GetInstance().PlayConfirmSound();

    if(questionList.Count > 0){
      Question currentQuestion = questionList[0];
      GlobalGameData.GetInstance().currentQuestion = currentQuestion;
      Persistence.GetInstance().SetVariableValue("current_character", currentQuestion.character);
      Persistence.GetInstance().SetVariableValue("current_place", currentQuestion.place);
      Persistence.GetInstance().SetVariableValue("current_answer", currentQuestion.answer);
      if(currentQuestion.introduction != "") {
        Persistence.GetInstance().SetVariableValue("current_question_has_intro", 1);
      } else {
        Persistence.GetInstance().SetVariableValue("current_question_has_intro", 0);
      }
      VSNController.GetInstance().StartVSNScript("VSN_start_dialog");
    }else{
      GlobalGameData.GetInstance().currentQuestion = null;
      VSNController.GetInstance().StartVSNScript(defaultVsnScript);
    }
    Utilities.Util.UnselectButton();
  }



  public void UpdateQuestionIcons(){
//    Debug.Log("Update Icons called for Place " + placeId);

    for(int i=0; i<2; i++){
      if(questionList.Count > i){
        questionIcon[i].SetActive(true);
      }else{
        questionIcon[i].SetActive(false);
      }
    }
  }

  public void AddQuestion(Question newQuestion){
    questionList.Add(newQuestion);
    UpdateQuestionIcons();
  }

  public Question PopQuestion(){
    Question q = questionList[0];
    questionList.RemoveAt(0);
    UpdateQuestionIcons();
    return q;
  }

  public void SetQuestionIconsVisible(bool value){
    foreach(GameObject icon in questionIcon){
      Image img = icon.GetComponent<Image>();
      img.color = new Color(img.color.r, img.color.g, img.color.b, value ? 1f : 0f);
    }
  }

  public void SetOutlineBlink(bool value){
    Outline outline = GetComponent<Outline>();
    if(value == true) {
      blinkTweener = outline.DOFade(1f, 0.6f).SetLoops(-1, LoopType.Yoyo);
    } else {
      if(blinkTweener != null) {
        blinkTweener.Kill();
        blinkTweener = null;
        outline.effectColor = new Color(outline.effectColor.r, outline.effectColor.g, outline.effectColor.b, 0f);
      }
    }
  }
}
