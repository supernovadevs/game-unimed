﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

[ExecuteInEditMode]
public class VersionIdentifier : MonoBehaviour {

  public Text versionText;
  
  void Update () {
    string versionPath = Application.dataPath + "/../version.txt";
    int currentVersion;

    if(Application.isEditor){
      if(File.Exists(versionPath)){
      currentVersion = int.Parse(File.ReadAllText(versionPath)) +1;

        versionText.text = "Versão: 0." + currentVersion;
      }else{
        versionText.text = "Versão: ???";
      }
    }
	}

  void GetVersion(){
    string versionPath = Application.dataPath + "/../version.txt";
    int currentVersion;

    currentVersion = int.Parse(File.ReadAllText(versionPath));
    currentVersion++;
    File.WriteAllText(versionPath, currentVersion.ToString());
  }
}
