﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSparksManager : MonoBehaviour {
  
	private static GameSparksManager instance = null;
	
  void Awake() {
		if(instance == null){
			instance = this; // if not, give it a reference to this class...
      Debug.Log("READY TO GET ACHIEVES");
      GameSparks.Api.Messages.AchievementEarnedMessage.Listener += AchievementMessageHandler;
			DontDestroyOnLoad(gameObject); // and make this object persistent as we load new scenes
		} else {
			Destroy(gameObject);
		}
	}


  void AchievementMessageHandler(GameSparks.Api.Messages.AchievementEarnedMessage message) {
    Debug.LogWarning("AWARDED ACHIEVEMENT: " + message.AchievementName + "!");

    GlobalGameData.GetInstance().achievementsList.Add( message.AchievementShortCode );
    AchievementsManager.GetInstance().AddNewAchievement( message.AchievementName );
  }




  public static GameSparksManager GetInstance(){
    return instance;
  }


  public void GetUserScore(){
    StartCoroutine(SendGetUserScoreRequest());
  }

  public IEnumerator SendGetUserScoreRequest(){
    List<string>leaderboardsList = new List<string>{"scoreLeaderboard"};
    string userId = GlobalGameData.GetInstance().userId;

    Debug.LogWarning("Getting user score now!");

    new GameSparks.Api.Requests.GetLeaderboardEntriesRequest()
      .SetPlayer(userId)
      .SetLeaderboards(leaderboardsList)
      .Send((response) => {
        if( response.BaseData.GetGSData("scoreLeaderboard").ContainsKey("score") ){
          GlobalGameData.GetInstance().userScore = (int)response.BaseData.GetGSData("scoreLeaderboard").GetFloat("score");
          Debug.LogWarning("Got user score! It's " + GlobalGameData.GetInstance().userScore );
        }else{
          Debug.LogWarning("User has no entry in the scoreLeaderboard!");
          GlobalGameData.GetInstance().userScore = 0;
        }
        GameMapManager.GetInstance().FinishLoadingUserInformation();
      });
    yield return null;
  }


  public void PlayerEarnPoints(int points){
    string userId = GlobalGameData.GetInstance().userId;
    GlobalGameData.GetInstance().userScore += points;

//    StartCoroutine(SendUpdateLeadeboardRequest());
  }

//  public IEnumerator SendUpdateLeadeboardRequest(){
//    Debug.Log("Updating leaderboard to "+GlobalGameData.GetInstance().userScore+" points!");
//
//    new GameSparks.Api.Requests.LogEventRequest()
//      .SetEventKey("scoreLeaderboardEvent")
//      .SetEventAttribute("score", GlobalGameData.GetInstance().userScore)
//      .Send((response) => {
//        if(!response.HasErrors){
//          Debug.LogWarning("Leaderboard updated!");
//        }else{
//          Debug.LogError("Error updating leaderboard!");
//        }
//      });
//    yield return null;
//  }

  public void SendPlayerPicture(string pictureId){
    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey("SetPlayerPicture")
      .SetEventAttribute("uploadId", pictureId)
      .Send((response) =>
      {
        if(!response.HasErrors){
          Debug.LogWarning("Updated picture correctly!");
          if(PhotoController.GetInstance() != null){
            PhotoController.GetInstance().FinishPhotoUpload();
          }
        }else{
          Debug.LogError("Error updating picture!");
        }
      });
  }
}
