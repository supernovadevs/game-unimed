using System;

[System.Serializable]
public class Question{
  public string id;
  public string introduction;
  public string text;
  public string[] choices;
  public int answer;
  public int theme;
  public int character;
  public int place;
  public string date;
}
