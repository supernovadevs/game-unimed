﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using Utilities;
using ImageAndVideoPicker;

public class DashboardManager : MonoBehaviour {

  public static DashboardManager instance;

  public Text userNameText;
  public Text userPositionText;
  public Text scoreText;
  public Text nextLevelScoreText;
  public Image userPhotoImage;
  public Slider nextLevelSlider;

  public RectTransform achievementsContentPanel;
  public GameObject achievementEntryPrefab;
  public GameObject achievementLockedEntryPrefab;
  public GameObject photoLoaderObject;

	public Texture2D t2d;


  void Awake(){
    instance = this;
  }

	void Start(){
		
	}

  public static DashboardManager GetInstance(){
    return instance;
  }


  public void OpenDashboard(){
    SoundManager.GetInstance().PlaySfx("OpenMenu");
    AchievementsManager.GetInstance().SendLogEventOpenDashboard();

    PopulateAchievementsPanel();
    UpdateUserData();
    gameObject.SetActive(true);
    Utilities.Util.UnselectButton();

    VSNController.GetInstance().StartVSNScript("VSN_tutorial_dashboard");
  }

  public void CloseDashboard(){
    SoundManager.GetInstance().PlaySfx("CloseMenu");
    gameObject.SetActive(false);
  }


  public void PopulateAchievementsPanel(){
    int numberOfAchievements = 32;
    float entryHeight = achievementEntryPrefab.GetComponent<RectTransform>().sizeDelta.y;
    GameObject newEntry;

    for(int i=0; i<numberOfAchievements; i++){
      newEntry = InstantiateAchievementEntry(i+1);
      newEntry.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -(float)i*entryHeight);
    }

    achievementsContentPanel.sizeDelta = new Vector2(achievementsContentPanel.sizeDelta.x, (float)numberOfAchievements*entryHeight);
  }

  GameObject InstantiateAchievementEntry(int achievementId){
    GameObject newEntry;

    if( AchievementsManager.GetInstance().IsAchievementUnlocked(achievementId) ) {
      newEntry = Instantiate(achievementEntryPrefab);
    } else {
      newEntry = Instantiate(achievementLockedEntryPrefab);
    }

    newEntry.transform.SetParent(achievementsContentPanel);
    newEntry.transform.localScale = Vector3.one;
    newEntry.GetComponent<AchievementEntry>().UpdateAchievementId(achievementId);

    return newEntry;
  }


  public void UpdateUserData(){
    int positionId = Util.GetPositionIdByPoints(GlobalGameData.GetInstance().userScore);
    int pointsToNextLevel = Util.PointsToGetNextPosition(positionId);
    int pointsRemaining = GlobalGameData.GetInstance().userScore - Util.PointsToGetThisPosition(positionId);

    userNameText.text = Util.GetFormatedDisplayName( GlobalGameData.GetInstance().userDisplayName );
    scoreText.text = GlobalGameData.GetInstance().userScore.ToString();
    userPositionText.text = Util.GetPositionNameById(positionId);
    nextLevelScoreText.text = (pointsToNextLevel != -1) ? pointsToNextLevel.ToString() : "---";
    nextLevelSlider.value = (pointsToNextLevel != -1) ? (float)pointsRemaining / (float)(pointsToNextLevel + pointsRemaining) : 0f;
    UpdateUserPhoto();
  }


  public void UpdateUserPhoto(){
    userPhotoImage.sprite = GlobalGameData.GetInstance().userPhoto;
  }

  public void OpenPhotoLoader(){
    //SceneManager.LoadScene(SceneNames.PhotoLoader.ToString());
    photoLoaderObject.SetActive(true);
  }

  public void ClosePhotoLoader(){
    //SceneManager.LoadScene(SceneNames.PhotoLoader.ToString());
    photoLoaderObject.SetActive(false);
  }
}
