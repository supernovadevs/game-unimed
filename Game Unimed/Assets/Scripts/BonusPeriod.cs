﻿using System;

[System.Serializable]
public class BonusPeriod {
  public string title;
  public string description;
  public string startDate;
  public string endDate;
  public int bonus;
}
