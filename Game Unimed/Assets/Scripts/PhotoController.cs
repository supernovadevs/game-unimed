﻿using GameSparks.Api.Requests;
using GameSparks.Api.Messages;
using ImageAndVideoPicker;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Utilities;
using System.Collections;
using System.Runtime.InteropServices;


public class PhotoController : MonoBehaviour {

  public static PhotoController instance;
  public Image previewImage;
  public GameObject errorMessageObject;
  public Text errorMessageText;
  public Slider progressBar;

  public Texture2D userPhotoTexture;

  static string s_dataUrlPrefix = "data:image/png;base64,";

  #if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void getImageFromBrowser(string objectName, string callbackFuncName);
  #endif


  void Awake(){
    instance = this;
    PickerEventListener.onImageLoad += OnImageLoadMobile;
    PickerEventListener.onImageSelect += OnImageSelectMobile;
    PickerEventListener.onError += OnErrorMobile;
    PickerEventListener.onCancel += OnCancelMobile;

    UploadCompleteMessage.Listener += GetUploadMessage;
  }

  void Start(){
    LoadPhoto();
  }


  public static PhotoController GetInstance(){
    return instance;
  }

  void OnCancelMobile (){}

  void OnErrorMobile (string err){}

  void OnImageSelectMobile (string path, ImageOrientation rotation){}

  public void LoadPhoto(){
    Debug.Log("LOAD NEW PHOTO");

    #if UNITY_ANDROID
      AndroidPicker.BrowseImage(false);
    #elif UNITY_IOS
      IOSPicker.BrowseImage(false);
    #elif UNITY_WEBGL
    if(Screen.fullScreen){
      ShowMessage("Enviar fotos não é suportado em modo tela cheia. Por favor pressione Esc para sair do modo tela cheia e escolher sua foto.");
    }else{
      GetImageFromUserAsync(gameObject.name, "OnImageLoadWebGL");
    }
    #endif
  }

  static public void GetImageFromUserAsync(string objectName, string callbackFuncName){
    #if UNITY_WEBGL
      getImageFromBrowser(objectName, callbackFuncName);
    #else
      Debug.LogError("Not supported in this platform");
    #endif
  }

  public void OnImageLoadMobile(string imgPath, Texture2D texture, ImageOrientation rotation){
    
    if(Application.platform == RuntimePlatform.IPhonePlayer){
      if(rotation == ImageOrientation.RIGHT){
        texture = RotateTexture(texture, 90);
      }else if(rotation == ImageOrientation.DOWN){
        texture = RotateTexture(texture, 180);
      }else if(rotation == ImageOrientation.LEFT){
        texture = RotateTexture(texture, 270);
      }
    }
    TextureScale.Resize(texture, 256, 256, TextureScale.ResizeAlgorithm.Point);
			
    userPhotoTexture = texture;
 
    Sprite createdSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));

    SetPreviewImage(createdSprite);
  }


  static public Texture2D RotateTexture(Texture2D texture, int angleToRotate){
    int timesToRotate = angleToRotate / 90;

    Debug.Log("Times to rotate: " + timesToRotate);

    for(int i=0; i<timesToRotate; i++){
      Debug.Log("ROTATED");
      texture = RotateTexture90(texture);
    }

    return texture;
  }

  static public Texture2D RotateTexture90(Texture2D texture){
    Texture2D target = new Texture2D(texture.height, texture.width, texture.format, false);  //flip image width<>height, as we rotated the image, it might be a rect. not a square image

    Color32[] pixels = texture.GetPixels32(0);
    pixels = RotateTextureGrid(pixels, texture.width, texture.height);
    target.SetPixels32(pixels);
    target.Apply();

    //flip image width<>height, as we rotated the image, it might be a rect. not a square image

    return target;
  }


  static public Color32[] RotateTextureGrid(Color32[] tex, int width, int height){
    Color32[] ret = new Color32[width * height];      //reminder we are flipping these in the target

    for (int y = 0; y < height; y++){
      for (int x = 0; x < width; x++){
        ret[(height-1)-y + x * height] = tex[x + y * width];         //juggle the pixels around
      }
    }
    return ret;
  }


  public void OnImageLoadWebGL(string dataUrl) { // USED FOR WEBGL IMAGE LOADING
    if(dataUrl.StartsWith(s_dataUrlPrefix)) {
      byte[] pngData = System.Convert.FromBase64String(dataUrl.Substring(s_dataUrlPrefix.Length));

      Texture2D newTexture = new Texture2D(1, 1);
      if(newTexture.LoadImage(pngData)) {
        userPhotoTexture = newTexture;
        Sprite createdSprite = Sprite.Create(newTexture, new Rect(0, 0, 256, 256),  new Vector2(0.5f, 0.5f), 50f);

        SetPreviewImage(createdSprite);
      } else {
        Debug.LogError("could not decode image");
      }
    } else {
      Debug.LogError("Error getting image:" + dataUrl);
    }
  }

  void SetPreviewImage(Sprite createdSprite){
    previewImage.sprite = createdSprite;
//    if(DashboardManager.GetInstance() != null){
//      DashboardManager.GetInstance().UpdateUserPhoto();
//    }

    GetUploadUrl();
  }



  public void GetUploadUrl(){
    ShowMessage("Enviando foto... Por favor aguarde.");
    ShowProgressBar();

    new GameSparks.Api.Requests.GetUploadUrlRequest()
      .Send((response) =>
      {
        if(!response.HasErrors){
          StartCoroutine( UploadAFile(response.Url) );
        }else{
          //Debug.LogError("Error getting Url to upload!");
//          ShowMessage("Ocorreu um erro ao enviar sua foto. Por favor, tente novamente mais tarde.");
          HideMessage();
          HideProgressBar();
        }
      });
  }



  public IEnumerator UploadAFile(string uploadUrl){
		Debug.Log("Dimensions w/h: " + userPhotoTexture.width + " / " + userPhotoTexture.height);
		byte[] bytes = userPhotoTexture.EncodeToJPG(50);

    var postForm = new WWWForm();
    postForm.AddBinaryData("file", bytes, "userPhoto.jpg", "image/jpg");

    WWW uploadRequest = new WWW(uploadUrl, postForm);
    float progress = 0f;

    while(progress < 1f){
      progress = uploadRequest.uploadProgress;
      UpdateProgressBar(0.1f+progress*0.8f);
      yield return null;
    }

    // safety wait
    yield return uploadRequest;

    if (uploadRequest.error != null){
      Debug.Log(uploadRequest.error);
    }else{
      Debug.Log(uploadRequest.text);
    }
    yield return null;
  }

  public void GetUploadMessage(GSMessage message){
    string uploadId = message.BaseData.GetString("uploadId");

//    ShowErrorMessage("Enviando foto 2..");

    //Debug.Log("Received upload id: " + uploadId);

    GameSparksManager.GetInstance().SendPlayerPicture(uploadId);
  }



  public void FinishPhotoUpload(){
//    ShowMessage("Foto enviada com sucesso! Por favor clique em Confirmar para retornar ao jogo.");
    HideMessage();
    PhotoController.GetInstance().HideProgressBar();
    GlobalGameData.GetInstance().userPhoto = previewImage.sprite;
    DashboardManager.GetInstance().UpdateUserPhoto();
  }  




  public void ShowMessage(string errorMsg){
    errorMessageObject.SetActive(true);
    errorMessageText.text = errorMsg;
  }

  public void HideMessage(){
    errorMessageObject.SetActive(false);
  }


  public void ShowProgressBar(){
    progressBar.gameObject.SetActive(true);
  }

  public void UpdateProgressBar(float progress){
    progressBar.value = progress;
  }

  public void HideProgressBar(){
    progressBar.gameObject.SetActive(false);
    GoBackToGameplay();
  }



  public void GoBackToGameplay(){
//    SceneManager.LoadScene(SceneNames.GameMap.ToString());
    DashboardManager.GetInstance().ClosePhotoLoader();
  }
}
