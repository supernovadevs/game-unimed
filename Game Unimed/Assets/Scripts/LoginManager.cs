﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Utilities;

public class LoginManager : MonoBehaviour {

  static LoginManager instance;

  public string encryptionKey;

  public Button loginButton;
  public GameObject loadingIcon;
  public InputField usernameField;
  public InputField passwordField;

  public Image blackImage;
  public CanvasGroup errorMessageCanvasGroup;
  public Text errorMessageText;

  public bool doUnimedAuthentication = true;

  private string unimedAuthenticateUrl = "https://osb.unimedfortaleza.com.br/ldap/proxy/auth";
  private string userPassword;
  private bool isNewPlayer = false;


  void Awake() {
    instance = this;

    SoundManager.GetInstance().PlayMusicForToday();

    LoadSavedUsername();
    
		if (Application.isMobilePlatform){
			LoadSavedPassword();
		}
    

    if(Application.platform == RuntimePlatform.WebGLPlayer ||
       Application.platform == RuntimePlatform.WindowsEditor){
      if(usernameField.text == ""){
        EventSystem.current.SetSelectedGameObject(usernameField.gameObject);
      }else{
        EventSystem.current.SetSelectedGameObject(passwordField.gameObject);
      }
    }
  }

  public void LoadSavedUsername(){
    if( PlayerPrefs.HasKey("savedUsername") ){
      usernameField.text = PlayerPrefs.GetString("savedUsername");
    }
  }

  void LoadSavedPassword() {
    if (SecurePlayerPrefs.HasKey("savedPassword")){
      passwordField.text = SecurePlayerPrefs.GetString("savedPassword", encryptionKey);
    }
  }

  public void SaveUsername(){
    PlayerPrefs.SetString("savedUsername", usernameField.text);
  }  

  public void SavePassword(){		
    SecurePlayerPrefs.SetString("savedPassword", passwordField.text, encryptionKey);		
  }

  public static LoginManager GetInstance() {
    return instance;
  }


  public void ClickForgotPasswordButton(){
    ShowErrorMessage("Utilize o seu mesmo login e senha de acesso à rede Unimed Fortaleza.");
  }

  public void TryToLogin() {
    SoundManager.GetInstance().PlayConfirmSound();
    CleanErrorMessage();
    StoreUsernameAndPassword();

    if(IsValidInput(GlobalGameData.GetInstance().username, userPassword)) {
      //      AuthenticateViaGameSparks();
      DeactivateLoginButton();
      AuthenticateViaUnimed();
//      StartCoroutine(OldLogin(username, password));
    }else{
      ShowErrorMessage("Nome de usuário ou senha inválidos.\nNenhum dos dois pode ser vazio.");
    }

  }

  bool IsValidInput(string username, string password) {
    if(username == "" || password == "") {
      return false;
    }

    return true;
  }



  void AuthenticateViaGameSparks() {
    //Debug.Log("Send GameSparks authentication request...");
    new GameSparks.Api.Requests.AuthenticationRequest()
      .SetUserName(GlobalGameData.GetInstance().username)
      .SetPassword("default")
      .Send((response) => {
      if(!response.HasErrors){
        LoginGameSparksSuccess(response);
      } else {
        LoginGameSparksFail(response);
      }
    });
  }



  void LoginGameSparksSuccess(GameSparks.Api.Responses.AuthenticationResponse authResponse){
    //Debug.Log("Success authenticating to GameSparks!");
    
    if(isNewPlayer) {
      isNewPlayer = false;
      SendPlayerScriptData();
      // TODO: also send update player leaderboard request (with score of 0)
    } else {
      ProceedToGameplay();
    }
  }


  void SendPlayerScriptData(){
    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey("SetPlayerScriptData")
      .SetEventAttribute("occupation", GlobalGameData.GetInstance().userOccupation)
      .SetEventAttribute("userType", "player")
      .Send( (response) =>
      {
        if(!response.HasErrors){
          //Debug.Log("Successfully sent user scriptData to GameSparks");
          ProceedToGameplay();
        }else{ 
          ShowErrorMessage("Não foi possível estabelecer uma conexão. Verifique seu acesso a internet e tente novamente.");
          ActivateLoginButton();
        }
      });
  }



  void LoginGameSparksFail(GameSparks.Api.Responses.AuthenticationResponse response){
    //Debug.LogWarning("Failed authenticating to GameSparks.");

    if(response.Errors.GetString("DETAILS") == "UNRECOGNISED"){
      RegisterPlayerInGameSparks();
    }else if(response.Errors.GetString("DETAILS") == "LOCKED"){
      ShowErrorMessage("Este usuário está bloqueado temporariamente devido a múltiplas tentativas falhadas de login.");
      ActivateLoginButton();
    }else{
      ShowErrorMessage("Não foi possível estabelecer uma conexão. Verifique seu acesso a internet e tente novamente.");
      ActivateLoginButton();
    }
  }



  void AuthenticateViaUnimed() {
    //Debug.Log("Send Unimed authentication request...");

    if(doUnimedAuthentication) {
      StartCoroutine(SendUnimedAuthenticateRequest());
    } else {
      StoreUserPersonalData("Usuario Teste", "SETOR TESTE");
      LoginUnimedSuccess();
    }
  }

  IEnumerator SendUnimedAuthenticateRequest(){
    WWWForm form = new WWWForm();
    Dictionary<string, string> headers = form.headers;
    byte[] rawData = form.data;

    //Debug.LogWarning("Sending Unimed Auth Request!");

    headers["Authorization"] = "Basic " + System.Convert.ToBase64String(
      System.Text.Encoding.ASCII.GetBytes(GlobalGameData.GetInstance().username+":"+userPassword));

    WWW www = new WWW(unimedAuthenticateUrl, null, headers);
    yield return www;


    if(!GotError(www.error)){
      GetUserDetails(www.text);
      LoginUnimedSuccess();
    }else{
      //Debug.LogWarning("Error authenticating via Unimed");
    }  
  }

  bool GotError(string errorString){

    if(errorString == null){
      //Debug.LogWarning("Error string is null!");
      return false;
    }

    //Debug.LogWarning("Checking for errors: " + errorString);

    if(errorString.Length >= 3){
      if(errorString.Substring(0, 3) == "401") {
        ShowErrorMessage("Não foi possível validar seu login. Por favor, confira seus dados e tente novamente.");
        ActivateLoginButton();
        return true;
      } else if(errorString.Substring(0, 3) == "500") {
        ShowErrorMessage("Não foi possível validar seu login. Entre em contato com o suporte técnico.");
        ActivateLoginButton();
        return true;
      } else {
        ShowErrorMessage("Não foi possível acessar o serviço de autenticação. Por favor verifique sua conexão com a internet e tente novamente.");
        ActivateLoginButton();
        return true;
      }
    } 
//    else {
//      Debug.LogError("Failed authenticating to Unimed.");
//      ShowErrorMessage("Não foi possível acessar o serviço de autenticação. Por favor contate o administrador da rede.");
//      return true;
//    }
    return false;
  }

  void GetUserDetails(string xmlReturn){
    //Debug.Log("Interpreting the Xml!");

    string displayName = "";
    string occupationName = "";
    XmlDocument doc = new XmlDocument();
    doc.LoadXml(xmlReturn);

    displayName = "";
    occupationName = "";

    foreach(XmlNode node in doc.GetElementsByTagName("tns:displayName")){
      displayName = node.InnerText;
    }
    
    foreach(XmlNode node in doc.GetElementsByTagName("tns:description")){
      foreach(XmlNode son in node.ChildNodes){
        if(son.Name == "tns:value"){
          occupationName = son.InnerText;
        }
      }
    }

    StoreUserPersonalData(displayName, occupationName);
  }



  void LoginUnimedSuccess(){
    //Debug.Log("Success authenticating to Unimed!");
    AuthenticateViaGameSparks();
  }




  public void StoreUsernameAndPassword(){
    GlobalGameData.GetInstance().username = usernameField.text.Trim();
    userPassword = passwordField.text;
  }

  public void StoreUserPersonalData(string displayName, string occupationName){

    //Debug.Log("setting the display name to: \"" + displayName + "\" and the occupation to: \"" + occupationName + "\"");

    GlobalGameData.GetInstance().userDisplayName = displayName;
    GlobalGameData.GetInstance().userOccupation = occupationName;
  }


  public void RegisterPlayerInGameSparks(){
    Debug.Log("Registering new player: " + GlobalGameData.GetInstance().username);
    isNewPlayer = true;

    new GameSparks.Api.Requests.RegistrationRequest()
      .SetDisplayName(GlobalGameData.GetInstance().userDisplayName)
      .SetPassword("default")
      .SetUserName(GlobalGameData.GetInstance().username)
      .Send((response) => {
      if(!response.HasErrors) {
        //Debug.Log("Player Registered successfully!");

        GlobalGameData.GetInstance().userId = response.UserId;
        GlobalGameData.GetInstance().userScore = 0;
        GameSparksManager.GetInstance().PlayerEarnPoints(0);

        SendPlayerScriptData();
      } else {
        ShowErrorMessage("Houve um erro ao tentar criar sua conta no jogo. Favor tente novamente mais tarde ou contate o administrador.");
        ActivateLoginButton();
      }
    }
    );
  }



  void CleanErrorMessage(){
    errorMessageText.text = "";
    errorMessageCanvasGroup.alpha = 0f;
  }

  public void ShowErrorMessage(string errorMsg){
    SoundManager.GetInstance().PlayCancelSound();
    errorMessageText.text = errorMsg;
    errorMessageCanvasGroup.alpha = 1f;
  }

  void ProceedToGameplay() {
    SaveUsername();
    


		if (Application.isMobilePlatform){
    	SavePassword();
		}
    

    blackImage.DOFade(1f, 1f).OnComplete( ()=> {
      SceneManager.LoadScene(Utilities.SceneNames.GameMap.ToString());
    } );
  }

  void ActivateLoginButton(){
    loginButton.interactable = true;
    loadingIcon.SetActive(false);
  }

  void DeactivateLoginButton(){
    loginButton.interactable = false;
    loadingIcon.SetActive(true);
  }

  void Update(){
    EventSystem eventSystem = EventSystem.current;

    if(Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return)){
      TryToLogin();
    }

    if(Input.GetKeyDown(KeyCode.Tab)){
      if(eventSystem.currentSelectedGameObject == usernameField.gameObject){
        eventSystem.SetSelectedGameObject( passwordField.gameObject );
      }else if(eventSystem.currentSelectedGameObject == passwordField.gameObject){
        eventSystem.SetSelectedGameObject( usernameField.gameObject );
      }
    }

    if(Input.GetKeyDown(KeyCode.Escape)) {
      Application.Quit();
    }
  }
}
