﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnswerEntry{
  public string questionId;
  public bool isCorrect;
  public string dateAnswered;
  public string itemAnswered;
}


[System.Serializable]
public class PlayerHistory{
  public string playerId;
  public List<AnswerEntry> answers;
}
