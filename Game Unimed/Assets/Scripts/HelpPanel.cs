﻿using UnityEngine;
using System.Collections;

public class HelpPanel : MonoBehaviour {
	
  public void OpenHelpPanel(){
    gameObject.SetActive(true);
  }

  public void CloseHelpPanel(){
    gameObject.SetActive(false);
  }


  public void ClickButton(string script){
    CloseHelpPanel();
    LoadVsnScript(script);
  }


  public void LoadVsnScript(string script){
    VSNController.GetInstance().StartVSNScript(script);
  }
}
