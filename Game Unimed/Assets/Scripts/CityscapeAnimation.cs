﻿using UnityEngine;
using System.Collections;


public class CityscapeAnimation : MonoBehaviour {

  public MeshRenderer meshRenderer;
  public float mx = 0;
  public float my = 0;
  
  void Start()
  {
    meshRenderer = GetComponent<MeshRenderer>();
  }

  void Update()
  {
    Vector2 position = new Vector2(mx, my);
    meshRenderer.material.mainTextureOffset = position;
  }
}
