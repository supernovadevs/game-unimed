﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LeaderboardController : MonoBehaviour {

  public LeaderboardEntry[] leaderboardEntries;
  public int startsFrom = 0;
  public Button previousButton;
  public Button fastBackwardButton;
  public Button nextButton;
  public Button fastForwardButton;

  public Button myPositionButton;
  public Button globalRankingButton;

  public GameObject loadingIcon;

  private bool showMyPosition;

  static LeaderboardController instance;

	void Awake(){
    ClearLeaderboard();
    instance = this;
	}

  public static LeaderboardController GetInstance(){
    return instance;
  }

  public void OpenLeaderboard(){
    SoundManager.GetInstance().PlaySfx("OpenMenu");
    AchievementsManager.GetInstance().SendLogEventOpenLeaderboard();

    startsFrom = 0;
    gameObject.SetActive(true);
    ShowMyPositionLeaderboard();
    Utilities.Util.UnselectButton();

    VSNController.GetInstance().StartVSNScript("VSN_tutorial_leaderboard");
  }

  public void CloseLeaderboard(){
    SoundManager.GetInstance().PlaySfx("CloseMenu");
    gameObject.SetActive(false);
  }


  public void ShowMyPositionLeaderboard(){
    SoundManager.GetInstance().PlayConfirmSound();
    showMyPosition = true;
    globalRankingButton.interactable = true;
    myPositionButton.interactable = false;
    UpdateLeaderboard();
  }

  public void ShowGlobalLeaderboard(){
    SoundManager.GetInstance().PlayConfirmSound();
    showMyPosition = false;
    globalRankingButton.interactable = false;
    myPositionButton.interactable = true;
    UpdateLeaderboard();
  }


  public void ClickNextButton(){
    SoundManager.GetInstance().PlayConfirmSound();
    IncreaseInitialEntry(10);
    UpdateLeaderboard();
  }

  public void ClickFastForwardButton(){
    SoundManager.GetInstance().PlayConfirmSound();
    IncreaseInitialEntry(100);
    UpdateLeaderboard();
  }



  public void ClickPreviousButton(){
    SoundManager.GetInstance().PlayConfirmSound();
    IncreaseInitialEntry(-10);
    UpdateLeaderboard();
  }

  public void ClickFastBackwardButton(){
    SoundManager.GetInstance().PlayConfirmSound();
    IncreaseInitialEntry(-100);
    UpdateLeaderboard();
  }

  void IncreaseInitialEntry(int reduceBy){
    startsFrom +=reduceBy;
    if(startsFrom<0){
      startsFrom = 0;
    }
  }  



  public void ClearLeaderboard(){
    foreach(LeaderboardEntry le in leaderboardEntries){
      le.Clear();
    }
    previousButton.interactable = false;
    fastBackwardButton.interactable = false;
    nextButton.interactable = false;
    fastForwardButton.interactable = false;
    loadingIcon.SetActive(true);
  }

  public void UpdateLeaderboard(){
    ClearLeaderboard();
    if(showMyPosition) {
      StartCoroutine(GetMyPositionData());
    } else {
      StartCoroutine(GetGlobalLeaderboardData());
    }
	}

  public IEnumerator GetGlobalLeaderboardData(){
    Debug.Log("Sending GetLeaderboardData request!");

    new GameSparks.Api.Requests.LeaderboardDataRequest()
      .SetLeaderboardShortCode("scoreLeaderboard")
      .SetOffset(startsFrom)
      .SetEntryCount(11)
      .Send((response) => {
        if(!response.HasErrors){
          Debug.LogWarning("Got leaderboard data:");
          Debug.Log( response.JSONString );

          List<GameSparks.Core.GSData> usersData = response.BaseData.GetGSDataList("data");
          UpdateLeaderboardEntries(usersData);
        }else{
          Debug.LogError("Error loading leaderboard data!");
        }
      } );
    yield return null;
  }

  public IEnumerator GetMyPositionData(){
    Debug.Log("Sending GetLeaderboardData request!");

    new GameSparks.Api.Requests.AroundMeLeaderboardRequest()
      .SetLeaderboardShortCode("scoreLeaderboard")
      .SetEntryCount(5)
      .Send((response) => {
      if(!response.HasErrors){
        Debug.LogWarning("Got leaderboard data:");
        Debug.Log( response.JSONString );

        List<GameSparks.Core.GSData> usersData = response.BaseData.GetGSDataList("data");
        UpdateLeaderboardEntries(usersData);
      }else{
        Debug.LogError("Error loading leaderboard data!");
        loadingIcon.SetActive(false);
      }
    } );
    yield return null;
  }

  public void UpdateLeaderboardEntries(List<GameSparks.Core.GSData> usersData){
    int rank;
    int entriesToUpdate = Mathf.Min(usersData.Count, 10);

    if(usersData.Count == 0){
      if(startsFrom > 0){
        IncreaseInitialEntry(-10);
        UpdateLeaderboard();
      }else{
        Debug.LogError("NO ENTRY TO SHOW!");
      }
      return;
    }

    for(int i=0; i<entriesToUpdate; i++){
      rank = (int)usersData[i].GetFloat("rank");

      leaderboardEntries[i].UpdateEntry(rank, usersData[i].GetString("userName"), (int)usersData[i].GetFloat("score"));
    }
    FinishLoadingLeaderboard(usersData.Count);
  }


  public void FinishLoadingLeaderboard(int howManyEntriesLoaded){
    loadingIcon.SetActive(false);

    if(showMyPosition == false) {
      if(howManyEntriesLoaded >= 11){
        nextButton.interactable = true;
        fastForwardButton.interactable = true;
      }
      if(startsFrom > 0){
        previousButton.interactable = true;
        fastBackwardButton.interactable = true;
      }
    }
  }
}
