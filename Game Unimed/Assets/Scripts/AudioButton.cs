﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AudioButton : MonoBehaviour {

  public Toggle audioToggle;

  void Awake(){
    if(PlayerPrefs.GetInt("isAudioOn", 1) == 1) {
      audioToggle.isOn = true;
    } else {
      audioToggle.isOn = false;
    }
    UpdateVolume();
  }



  public void ClickVolumeButton(){
    PlayerPrefs.SetInt("isAudioOn", audioToggle.isOn ? 1 : 0);
    UpdateVolume();
    SoundManager.GetInstance().PlayConfirmSound();
  }

  void UpdateVolume(){
    SoundManager.GetInstance().TurnVolumeOn(audioToggle.isOn);
  }
}
