﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GlobalGameData : MonoBehaviour {

  public static GlobalGameData instance = null;

  public string username = "";
  public string userId;
  public string userDisplayName = "";
  public string userOccupation = "";
  public Sprite userPhoto;
  public int userScore;
  public Question currentQuestion;
  public BonusPeriod currentBonusPeriod = null;
  public List<string> achievementsList;

  public bool hasCheckedLeaderboard = false;
  public bool hasCheckedDashboard = false;
  public float timeQuestionAppeared;

  public List<string> loadedDates; // DEBUG: FOR CHECKING PLAYER HISTORY
  public PlayerHistory playerHistory;


  void Awake() {
    if(instance == null){
      instance = this;
      DontDestroyOnLoad(gameObject);
    } else {
      Destroy(gameObject);
    }
    LoadPlayerHistory();
  }

  public static GlobalGameData GetInstance(){
    return instance;
  }

  public void ResetData(){
    userPhoto = null;
    achievementsList = null;
    currentQuestion = null;
    currentBonusPeriod = null;
    hasCheckedLeaderboard = false;
    hasCheckedDashboard = false;
  }


  public void LoadPlayerHistory(){
    if(File.Exists("history.txt")){
      string t = File.ReadAllText("history.txt");

      playerHistory = JsonUtility.FromJson<PlayerHistory>(t);
      Debug.Log("loaded history successfully!");
    }
  }
}
