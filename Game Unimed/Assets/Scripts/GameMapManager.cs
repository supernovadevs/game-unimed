﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GameSparks.Api.Responses;
using GameSparks.Api.Requests;
using GameSparks.Api;
using GameSparks.Core;
using DG.Tweening;

public class GameMapManager : MonoBehaviour {

  public static GameMapManager instance;

  public Place[] placesArray;

  public LeaderboardController leaderboardController;
  public DashboardManager dashboardManager;
  private bool loadedUserInfo;
  private bool loadedBonusPeriod;
  private bool loadedQuestions;

  public GameObject[] tutorialArrows;


  public static GameMapManager GetInstance(){
    return instance;
  }


  void Awake() {
    instance = this;

    loadedUserInfo = false;
    loadedQuestions = false;
    loadedBonusPeriod = false;

    StartCoroutine(LoadUserInformation());
    StartCoroutine(LoadBonusPeriod());
    StartCoroutine(LoadQuestions());
  }

  void Start() {
    SoundManager.GetInstance().PlayMusicForToday();
  }


  void Update() {
    if(Input.GetKeyDown(KeyCode.Escape)) {
      if(leaderboardController.gameObject.activeSelf == true) {
        leaderboardController.CloseLeaderboard();
      } else if(dashboardManager.gameObject.activeSelf == true) {
        dashboardManager.CloseDashboard();
      }
    }
  }

  public void ClickSignOutButton() {
    SceneManager.LoadScene(Utilities.SceneNames.Login.ToString());
    GlobalGameData.GetInstance().ResetData();
  }



  IEnumerator LoadUserInformation() {
    //Debug.LogWarning("Starting to load user info");

    new AccountDetailsRequest()
      .Send((response) => {
      if(!response.HasErrors) {
        //Debug.LogWarning("User info retrieved!");

        GlobalGameData.GetInstance().userId = response.UserId;
        GlobalGameData.GetInstance().userDisplayName = response.DisplayName;
        GlobalGameData.GetInstance().achievementsList = response.Achievements;
        if(response.Achievements == null) {
          GlobalGameData.GetInstance().achievementsList = new List<string>();
        }
        SetVsnVariable("tutorial_1", response.ScriptData);
        SetVsnVariable("tutorial_2", response.ScriptData);
        SetVsnVariable("tutorial_3", response.ScriptData);
        SetVsnVariable("tutorial_4", response.ScriptData);
        SetVsnVariable("tutorial_5", response.ScriptData);
        SetVsnVariable("tutorial_6", response.ScriptData);
        SetVsnVariable("conhece_sofia", response.ScriptData);
        SetVsnVariable("conhece_arthur", response.ScriptData);
        SetVsnVariable("conhece_ana", response.ScriptData);
        SetVsnVariable("conhece_jose_carlos", response.ScriptData);
        SetVsnVariable("conhece_roberta", response.ScriptData);
        if(response.ScriptData.ContainsKey("pictureId") ){
          GetUserPhotoUrl(response.ScriptData.GetString("pictureId"));
        }
//        else{
//          Debug.LogWarning("PICTURE ID: NONE");
//        }

        Invoke("GetPlatformAchievement", 2f);

        GameSparksManager.GetInstance().GetUserScore();
      } else {
        //Debug.LogError("Error retrieving player info!");
      }
    });

    yield return null;
  }

  public static void SetVsnVariable(string varName, GSData scriptData){
    if(scriptData.ContainsKey(varName) ){
      Persistence.GetInstance().SetVariableValue(varName, 1 );
    }else{
      Persistence.GetInstance().SetVariableValue(varName, 0);
    }
  }


  IEnumerator LoadQuestions() {
    int i = 0;
    string logEventToCall = "GetUnansweredQuestionsFromToday";

    if(Persistence.debugMode){
      logEventToCall = "GetAllQuestions";
      CreateEmptyFile("todas_perguntas.txt");
    }

    //Debug.Log("Questions loading...");
    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey(logEventToCall)
			.Send((response) => {
      
      if(!response.HasErrors) {

        List<Question> questionList = new List<Question>();

        foreach(GSData questionData in response.ScriptData.GetGSDataList("QuestionArray")) {
          Question question = new Question();

          question.id = questionData.GetGSData("_id").GetString("$oid");
          if(questionData.ContainsKey("introduction")){
            question.introduction = questionData.GetString("introduction");
          }else{
            question.introduction = "";
          }
          question.text = questionData.GetString("text");
          question.choices = new string[3];
          for(int j = 0; j < 3; j++) {
            question.choices[j] = questionData.GetGSDataList("choices")[j].GetString("resposta");
          }
          question.answer = ConvertAnswerCharToInt(questionData.GetString("answer")[0]);
//          Debug.Log("Theme: " + questionData.GetString("theme"));
//          Debug.Log("Character: " + questionData.GetString("character"));
//          Debug.Log("Place: " + questionData.GetString("place"));

          question.theme = int.Parse(questionData.GetString("theme").Substring(0, 1));
          question.character = int.Parse(questionData.GetString("character").Substring(0, 1));
          question.place = int.Parse(questionData.GetString("place").Substring(0, 1));
          question.date = questionData.GetString("date");

          if(Persistence.debugMode){
            LogQuestionToTxt("todas_perguntas.txt", question, questionData);
          }

          questionList.Add(question);
        }

        //Debug.Log("Questions loaded successfully!");

        if(Persistence.debugMode){
          GetAllDates(questionList);
          CreateEmptyFile("daysAnswered.txt");
          CheckIfAnyDateHasNoAnswers();
        }

        AddQuestionsToPlaces(questionList);

        loadedQuestions = true;
        CheckIfLoadedAllInfo();

      } else {
        Debug.Log("Questions loading had errors");
      }
    });
    yield return null;
  }

  public void CreateEmptyFile(string pathName){
    File.Create(pathName).Close();
    string separator = "^";
    File.WriteAllText(pathName, "Introdução" + separator + 
                      "Pergunta" + separator + 
                      "Item A" + separator + 
                      "Item B" + separator + 
                      "Item C" + separator + 
                      "Resposta Correta" + separator + 
                      "Data" + separator + 
                      "Tema" + separator + 
                      "Personagem" + separator + 
                      "Local" + "\n", System.Text.Encoding.Unicode);
  }

  public void LogQuestionToTxt(string pathName, Question q, GSData qData){
    string questionContent = "";
    string separator = "^";
    questionContent += q.introduction + separator;
    questionContent += q.text + separator;
    questionContent += q.choices[0] + separator;
    questionContent += q.choices[1] + separator;
    questionContent += q.choices[2] + separator;
    questionContent += qData.GetString("answer") + separator;
    questionContent += qData.GetString("date") + separator;
    questionContent += qData.GetString("theme") + separator;
    questionContent += qData.GetString("character") + separator;
    questionContent += qData.GetString("place") + "\n";

    File.AppendAllText(pathName, questionContent, System.Text.Encoding.Unicode);
  }

  public void LogToTxt(string pathName, string msg){
    File.AppendAllText(pathName, msg+"\n", System.Text.Encoding.Unicode);
  }


  IEnumerator LoadBonusPeriod() {
    int i = 0;
    GlobalGameData.GetInstance().currentBonusPeriod = null;

    Debug.Log("Bonus Period loading...");

    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey("GetCurrentBonusPeriods")
      .Send((response) => {

      Persistence.GetInstance().SetVariableValue("has_bonus_period", 0);

      if(!response.HasErrors) {
        foreach(GSData questionData in response.ScriptData.GetGSDataList("BonusPeriodArray")) {
          BonusPeriod bonusPeriod = new BonusPeriod();

          bonusPeriod.title = questionData.GetString("title");
          bonusPeriod.description = questionData.GetString("description");
          bonusPeriod.startDate = questionData.GetString("startDate");
          bonusPeriod.endDate = questionData.GetString("endDate");
          bonusPeriod.bonus = int.Parse(questionData.GetFloat("bonus").ToString());

//          Debug.Log("Title: " + bonusPeriod.title);
//          Debug.Log("Description: " + bonusPeriod.description);
//          Debug.Log("Bonus: " + bonusPeriod.bonus);

          GlobalGameData.GetInstance().currentBonusPeriod = bonusPeriod;
          Persistence.GetInstance().SetVariableValue("has_bonus_period", 1);
          break;
        }

        Debug.LogWarning("Bonus Period loaded successfully!");

        loadedBonusPeriod = true;
        CheckIfLoadedAllInfo();

      } else {
        Debug.LogError("Bonus Period loading had errors");
      }
    });
    yield return null;
  }

  public void FinishLoadingUserInformation(){
    loadedUserInfo = true;
    CheckIfLoadedAllInfo();
  }  



  public void GetUserPhotoUrl(string pictureId){
    Debug.LogWarning("PICTURE ID: " + pictureId);

    new GameSparks.Api.Requests.GetUploadedRequest()
      .SetUploadId(pictureId)
      .Send((response)=>
      {
        if(!response.HasErrors){
          Debug.LogWarning("Success loading user photo URL: " + response.Url);
          StartCoroutine(GetUserPhoto(response.Url));
        }else{
          Debug.LogError("Error loading user photo URL");
        }
      });
  }


  public IEnumerator GetUserPhoto(string url){
    Debug.Log("Loading user photo from URL: " + url);

    WWW photoRequest = new WWW(url);
    yield return photoRequest;

    if(photoRequest.error == null || photoRequest.error == ""){
      Debug.LogWarning("Success downloading user photo!");

      Sprite userSprite = Sprite.Create(photoRequest.texture,
                                        new Rect(0, 0, photoRequest.texture.width, photoRequest.texture.height),
                                        new Vector2(0.5f, 0.5f));
      GlobalGameData.GetInstance().userPhoto = userSprite;
      dashboardManager.UpdateUserPhoto();
    }else{
      Debug.LogError("Error downloading user photo: '" + photoRequest.error + "'");
    }
  }



  void CheckIfLoadedAllInfo(){
    if(!loadedBonusPeriod || !loadedQuestions || !loadedUserInfo){
      return;
    }

    VSNController.GetInstance().StartVSNScript("VSN_intro");
  }


  public void GetPlatformAchievement() {
    if(Application.platform == RuntimePlatform.Android ||
       Application.platform == RuntimePlatform.IPhonePlayer ||
       Application.platform == RuntimePlatform.WP8Player) {
      AchievementsManager.GetInstance().SendGetAchievementRequest("unimedNaMao");
    }

    if(Application.platform == RuntimePlatform.WebGLPlayer ||
       Application.platform == RuntimePlatform.WindowsEditor) {
      AchievementsManager.GetInstance().SendGetAchievementRequest("unimedNoPC");
    }
  }


  public int ConvertAnswerCharToInt(char a) {
    switch(a) {
      case 'a':
        return 0;
      case 'b':
        return 1;
      case 'c':
        return 2;
    }
    return -1;
  }


  void AddQuestionsToPlaces(List<Question> questionList) {
		
    foreach(Question question in questionList) {
      for(int i = 0; i < 5; i++) {
        if(question.place == placesArray[i].placeId) {
          //Debug.LogWarning("Adding question: " + question.text);
          placesArray[i].AddQuestion(question);
          continue;
        }
      }
    }
  }

  public void ShowTutorialArrow(int arrowId, bool value){
    tutorialArrows[arrowId-1].SetActive(value);
  }

  public void GetAllDates(List<Question> allQuestions){
    GlobalGameData.instance.loadedDates = new List<string>();
    foreach(Question q in allQuestions){
      if(!GlobalGameData.instance.loadedDates.Contains(q.date)){
        GlobalGameData.instance.loadedDates.Add(q.date);
      }
    }
  }

  public void CheckIfAnyDateHasNoAnswers(){
    int count = 0;
    foreach(string date in GlobalGameData.instance.loadedDates){
      count += QuestionsAnsweredThisDay(date);
      string s = "Answers day " + date + ": " + QuestionsAnsweredThisDay(date);
      Debug.Log(s);
      LogToTxt("daysAnswered.txt", s);
    }
    Debug.Log("Total answers: " + count);
  }

  public int QuestionsAnsweredThisDay(string date){
    int count = 0;
    foreach(AnswerEntry e in GlobalGameData.instance.playerHistory.answers){
      if(e.dateAnswered == date){
        count++;
      }
    }
    return count;
  }
}
