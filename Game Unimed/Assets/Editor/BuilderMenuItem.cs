﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class BuilderMenuItem : MonoBehaviour {

	// All the scenes you want to build (order is relevant)
	static string [] scenes = { "Assets/Scenes/TestScene.unity" };

	[MenuItem("Supernova Build/Build Android")]
	static void BuildAndroid(){

    PlayerSettings.applicationIdentifier = "com.mycompany.mygame"; // Android Bundle Identifier

		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
		BuildPipeline.BuildPlayer(scenes, "android_app_name.apk",
			BuildTarget.Android, BuildOptions.None);
	}

	[MenuItem("Supernova Build/Build iOS")]
	static void BuildIOS(){

    PlayerSettings.applicationIdentifier = "com.mycompany.mygame"; // iOS Bundle Identifier

		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.iOS);
		BuildPipeline.BuildPlayer(scenes, "ios_app_name.app",
			BuildTarget.iOS, BuildOptions.None);
	}

}
