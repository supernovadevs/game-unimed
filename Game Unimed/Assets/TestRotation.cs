﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TestRotation : MonoBehaviour {

  public Texture2D baseTexture;
  public Image testImage;
  public int angleToRotate;
  private int savedAngle = -1;


	void Start () {
//    RotateImage();
    StartCoroutine(CheckRotate());
	}

  IEnumerator CheckRotate(){
    while(true) {
      if(savedAngle != angleToRotate) {
        RotateImage();
        savedAngle = angleToRotate;
      }
      yield return new WaitForSeconds(0.2f);
    }
  }

  void RotateImage(){
    Texture2D newTexture;
    newTexture = PhotoController.RotateTexture(baseTexture, angleToRotate);
    Sprite createdSprite = Sprite.Create(newTexture, new Rect(0, 0, newTexture.width, newTexture.height), new Vector2(0.5f, 0.5f));

    testImage.sprite = createdSprite;
  }
}
