using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class VSNController : MonoBehaviour {
	
  private static VSNController instance;

  public GameState gameState = GameState.PlayingScript;
//  public TextAsset[] scriptList = new TextAsset[4];
  public int chapterId = -1;
  public int lineStarted = -1;
  public int checkpointStarted = -1;
  public int currentLine = -1;
  public int lineCount;

  public Sprite bgTemp { get; set; }

  //	  public Quit quit;
  public CharacterList charList;

  public DialogBox dialogBox;
  public DialogScreen screen;
  public VSNScriptReader reader;
  //  public Inventory inventory;

  private int[] choices;
  private int testimonyNextWp;
  private string[] labelNames;
  private string questionName;
  private bool waitForConfirmation;
  private string sendAnswerConnectionErrorWaypoint;
  private string sendAnswerDuplicateErrorWaypoint;


  void Awake() {
    dialogBox.gameObject.SetActive(false);
    instance = this;
  }

  public static VSNController GetInstance() {
    return instance;
  }



  public enum GameState {
    PlayingScript,
    Dialog,
    Question,
    Collection,
    Idle,
    WaitingForConfirmation,
    Inventory,
    ChooseTopic,
    Paused
  }

	
  void Start() {
    // load the character animations here
    //Persistence.DeleteCharacterAnims();
    //Persistence.GetInstance().CreateCharacterAnims();

    if(gameState == GameState.PlayingScript)
      StartVSN();
  }

  void Update() {
    CheckToPlayScript();

    if(Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return) ||
       Input.GetKeyDown(KeyCode.Space)){
      ClickedScreen();
    }
  }



  public void StartVSN(){
    Debug.Log("Starting VSN");
    StartVSNScript("VSN_example_script");
  }

  public void StartVSNScript(string scriptToLoad){
    //    dialogBox.gameObject.SetActive(true);
    screen.gameObject.SetActive(true);
    gameState = GameState.PlayingScript;


    TextAsset asset = Resources.Load<TextAsset>(scriptToLoad);
    if(asset == null){
      Debug.LogError("Error loading VSN Script: " + scriptToLoad);
    }
    reader.SetCurrentScript( asset );



    DebugMode.gameController = this;

    reader.LoadScript(checkpointStarted);

    lineStarted = reader.checkpoints [checkpointStarted];
    VSNScriptReader.GetInstance().GoToLine(lineStarted);
  }

  public void ResumeVSN() {
    gameState = GameState.PlayingScript;
    screen.gameObject.SetActive(true);
  }

  public void PauseVSN() {
    gameState = GameState.Paused;
    screen.gameObject.SetActive(false);
  }





  public void ClickedScreen() {
    Utilities.Util.UnselectButton();

    switch(gameState) {
    case GameState.Dialog:
      AdvanceDialog();
      break;
    case GameState.Question:
      SkipDialog();
      break;
    }
  }

  void CheckToPlayScript() {
    if(gameState == GameState.PlayingScript) {
      PlayScript();
    }
  }

  void PlayScript() {
    reader.ReadScript();
  }

  void AdvanceDialog() {
    if(SkipDialog())
      return;
    if(NextDialog())
      return;
  }

  public void ChooseOption(int value) {
    SkipDialog();
    AnswerQuestion(value);
  }

  public void SetQuestion(string question) {
    questionName = question;
  }

  public void SetLabelNames(string[] names) {
    labelNames = names;
  }

  void AnswerQuestion(int choiceIndex) {
    VSNScriptReader.GetInstance().GoToLine(choices [choiceIndex]);
    gameState = GameState.PlayingScript;
  }

  public void GoToSelectedWaypoint(int wp) {
    VSNScriptReader.GetInstance().GoToLine(wp);
    gameState = GameState.PlayingScript;
  }

  public bool SkipDialog() {
    if(dialogBox.talking) {
      dialogBox.SkipDialog();
      return true;
    }
    return false;
  }

  public bool NextDialog() {
    if(!dialogBox.talking) {
      //screen.StopAnimation();
      dialogBox.ShowIndicatorArrow(false);
      gameState = GameState.PlayingScript;
      return true;
    }
    return false;
  }

  public void SetQuestionState(int[] choicesindex) {
    choices = choicesindex;
    gameState = GameState.Question;
  }

  public void SetDialogState() {
    gameState = GameState.Dialog;
  }

  public void SetCollectionState() {
    gameState = GameState.Collection;
  }

  public void SetChooseTopicState() {
    gameState = GameState.ChooseTopic;
  }

  public void WaitForItem() {
    gameState = GameState.Idle;
  }

  public void WaitSeconds(float time) {
    StartCoroutine("Wait", time);
  }

  IEnumerator Wait(float time) {
    gameState = GameState.Idle;
    yield return new WaitForSeconds(time);
    gameState = GameState.PlayingScript;
  }

  public void WaitForValidation(string connectionErrorWaypoint, string duplicateErrorWaypoint) {
    sendAnswerConnectionErrorWaypoint = connectionErrorWaypoint;
    sendAnswerDuplicateErrorWaypoint = duplicateErrorWaypoint;
    StartCoroutine("WaitForAnswerConfirm");
  }

  IEnumerator WaitForAnswerConfirm() {
    gameState = GameState.WaitingForConfirmation;
    while(gameState == GameState.WaitingForConfirmation){
      yield return null;
    }
    //gameState = GameState.PlayingScript;
  }

  public void SendAnswerConfirmation(){
    gameState = GameState.PlayingScript;
  }

  public void SendAnswerConnectionError(){
    if(VSNCommands.GetInstance().waypoints.ContainsKey(sendAnswerConnectionErrorWaypoint)) {
      int lineNumber = VSNCommands.GetInstance().waypoints[sendAnswerConnectionErrorWaypoint];
      VSNScriptReader.GetInstance().GoToLine(lineNumber);
    }else{
      Debug.Log("ERROR SENDING ANSWER ERROR");
    }  
    gameState = GameState.PlayingScript;
  }

  public void SendAnswerDuplicateError(){
    if(VSNCommands.GetInstance().waypoints.ContainsKey(sendAnswerDuplicateErrorWaypoint)) {
      int lineNumber = VSNCommands.GetInstance().waypoints[sendAnswerDuplicateErrorWaypoint];
      VSNScriptReader.GetInstance().GoToLine(lineNumber);
    }else{
      Debug.Log("ERROR SENDING ANSWER ERROR");
    }  
    gameState = GameState.PlayingScript;
  }




  public void State_Inventory() {
    gameState = GameState.Inventory;
  }

  public void ReloadLevel() {
    Persistence.Load(chapterId, 0);
  }

  public void AdvanceChapter() {
    Persistence.SetChapterAccess(chapterId + 1, 1);
    Persistence.ReachCheckpoint(chapterId + 1, 0);
    Persistence.Load(chapterId + 1, 0);
  }


  public void ExitGame() {
    Application.Quit();		
  }
}
