﻿using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DG.Tweening;
using Utilities;

public class VSNCommands : MonoBehaviour {

  public static VSNCommands instance;

  public Dictionary<string, int> waypoints = new Dictionary<string, int>();

  public DialogScreen screen;
  public Fade fade;


  void Awake(){
    instance = this;
  }

  public static VSNCommands GetInstance(){
    return instance;
  }


  public bool CommandBreaksReading(string line) {
    string command = GetCommand(line);

    if(command == "wait" || command == "say" || command == "say_bonus" || command == "current_introduction" || command == "choices" ||
       command == "current_choices" || command == "collect" || command == "fade_out" || command == "fade_in" ||
       command == "minigame_start" || command == "to_start_menu" || command == "topic_new" || command == "wait_answer_confirm" ||
       command == "end_script" || command == "earn_points")
      return true;
    else
      return false;
  }

  public void CheckCommand(string line, int lineNumber) {
		
    //Debug.log("line " + lineNumber+": "+line );
    string command = GetCommand(line);
    string[] param = GetParams(line);
    VSNScriptReader.GetInstance().GoToLine(lineNumber);
		
    switch(command) {
      case "player_prefs":
        Player_Prefs_Command(param);
        break;
      case "wait":
        Wait_Command(param);
        break;
      case "wait_answer_confirm":
        WaitAnswerConfirm_Command(param);
        break;
      case "character":
        Character_Command(param);
        break;
      case "character_reset_all":
        Character_Reset_All_Command();
        break;
      case "char_sprite":
        Char_Sprite_Command(param);
        break;
      case "char_base":
        Char_Base_Command(param);
        break;
      case "char_animate":
        CharAnimate_Command(param);
        break;
      case "bg":
        Bg_Command(param);
        break;
      case "fg":
        Fg_Command(param);
        break;
      case "move_x":
        Move_x_Command(param);
        break;
      case "move_y":
        Move_y_Command(param);
        break;
      case "mouth_anim":
        Mouth_Anim_Command(param);
        break;
      case "eye_blink_anim":
        Eye_Blink_Anim_Command(param);
        break;
      case "anim_alpha":
        AnimAlpha_Command(param);
        break;
      case "anim_scale":
        Anim_Scale_Command(param);
        break;
      case "music":
        Music_Command(param);
        break;
      case "ambience":
        Ambience_Command(param);
        break;
      case "fade_out_music":
        Fade_Out_Music_Command(param);
        break;
      case "sfx":
        Sfx_Command(param);
        break;
      case "say":
        Say_Command(param);
        break;
      case "say_bonus":
        SayBonus_Command(param);
        break;
      case "say_sfx":
        Say_Sfx_Command(param);
        break;
      case "say_pitch":
        Say_Pitch_Command(param);
        break;
      case "question":
        Question_Command(param);
        break;
      case "choices":
        Choices_Command(param);
        break;
      case "pop_question":
        PopQuestion_Command();
        break;


      case "current_introduction":
        CurrentIntroduction_Command();
        break;
      case "current_question":
        CurrentQuestion_Command();
        break;
      case "current_choices":
        CurrentChoices_Command();
        break;
      case "log_answer_event":
        LogAnswerEvent_Command(param);
        break;
      case "add_script_data":
        AddScriptData_Command(param);
        break;

      case "goto":
        Goto_Command(param);
        break;
      case "waypoint":
        break;
      case "screenshake":
        Screenshake_Command(param);
        break;
      case "fade_in":
        Fade_In_Command(param);
        break;
      case "fade_out":
        Fade_Out_Command(param);
        break;
      case "fade_to":
        Fade_To_Command(param);
        break;
      case "mirror":
        Mirror_Command(param);
        break;
      case "add_var":
        Add_Var_Command(param);
        break;
      case "if":
        If_Command(param);
        break;
      case "else":
        Else_Command();
        break;
      case "earn_points":
        EarnPoints_Command();
        break;
      case "set_var":
        Set_Var_Command(param);
        break;
      case "save_game":
        Save_Game_Command(param);
        break;
      case "set_number_victories":
        Set_Number_Victories(param);
        break;
      case "checkpoint":
        Checkpoint_Command(param);
        break;
      case "to_next_chapter":
        To_Next_Chapter_Command();
        break;
      case "end_script":
        EndScript_Command();
        break;
      case "goto_script":
        GotoScript_Command(param);
        break;
      case "check_quick_answer":
        CheckQuickAnswer_Command();
        break;
      case "start_count_time":
        StartCountTime_Command();
        break;
      case "show_loading_icon":
        ShowLoadingIcon_Command(param);
        break;
      case "show_victory_screen":
        ShowVictoryScreen_Command(param);
        break;
      case "show_tutorial_icon":
        ShowTutorialIcon_Command(param);
        break;
      case "set_question_icons_visible":
        SetQuestionIconsVisible_Command(param);
        break;
      case "set_places_outline_blink":
        SetPlacesOutlineBlink_Command(param);
        break;
      default:
  			//Debug.log("WRONG COMMAND: " + command);
        break;
    }
  }

  void EndScript_Command() {
    Debug.Log("Calling endscript");
    VSNController.GetInstance().PauseVSN();
  }

  void GotoScript_Command(string[] param) {
    EndScript_Command();
    VSNController.GetInstance().StartVSNScript(param[0]);
  }

  void CheckQuickAnswer_Command() {
    float timeToAnswer = GlobalGameData.GetInstance().timeQuestionAppeared - Time.time;

    if(timeToAnswer <= 60f) {
      AchievementsManager.GetInstance().SendLogEventAnswerQuickly();
    }
  }

  void StartCountTime_Command() {
    GlobalGameData.GetInstance().timeQuestionAppeared = Time.time;
  }

  void ShowLoadingIcon_Command(string[] param){
    screen.SetLoadingIcon(param[0]=="true" ? true : false);
  }

  void ShowVictoryScreen_Command(string[] param){
    screen.SetVictoryScreen(param[0]=="true" ? true : false);
  }

  void ShowTutorialIcon_Command(string[] param){
    GameMapManager.GetInstance().ShowTutorialArrow(int.Parse(param[0]), param[1]=="true" ? true : false);
  }

  void SetQuestionIconsVisible_Command(string[] param){
    foreach(Place p in GameMapManager.GetInstance().placesArray) {
      if(param[0] == "true") {
        p.SetQuestionIconsVisible(true);
      } else if(param[0] == "false") {
        p.SetQuestionIconsVisible(false);
      } else {
        Debug.Log("Wrong argument used.");
        break;
      }
    }
  }

  void SetPlacesOutlineBlink_Command(string[] param){
    foreach(Place p in GameMapManager.GetInstance().placesArray) {
      if(param[0] == "true") {
        p.SetOutlineBlink(true);
      } else if(param[0] == "false") {
        p.SetOutlineBlink(false);
      } else {
        Debug.Log("Wrong argument used.");
        break;
      }
    }
  }

  public static void Player_Prefs_Command(string[] param) {
    PlayerPrefs.SetString(param[0], param[1]);
  }

  public void Save_Game_Command(string[] param) {
    //Persistence.GetInstance().SaveGame();
  }

  public static void CheckAnimCommand(string line, int lineNumber) {
		
    string command = GetStaticCommand(line);
    string[] param = GetParams(line);
		
    switch(command) {
      case "mouth_anim":
        Mouth_Anim_Command(param);
        break;
      case "eye_blink_anim":
        Eye_Blink_Anim_Command(param);
        break;
      default:
        break;
    }
  }

  void Wait_Command(string[] waitTime) {
    float time = float.Parse(waitTime[0]);
    screen.EnableDialogBox(false);
    screen.choices.SetActive(false);
    VSNController.GetInstance().WaitSeconds(time);
  }

  void WaitAnswerConfirm_Command(string[] param){
    screen.EnableDialogBox(false);
    screen.choices.SetActive(false);
    VSNController.GetInstance().WaitForValidation(param[0], param[1]);
  }
    

  void Music_Command(string[] args) {
//    AudioController.GetInstance().PlayMusic(args[0]);
  }

  void Ambience_Command(string[] args) {
//    AudioController.GetInstance().PlayAmbience(args[0], float.Parse(args[1]));
  }

  void Fade_Out_Music_Command(string[] args) {
//    AudioController.GetInstance().FadeMusic(float.Parse(args[0]));
  }

  void Sfx_Command(string[] args) {
//    AudioController.GetInstance().PlaySfx(args[0]);
    SoundManager.GetInstance().PlaySfx(args[0]);
  }

  void Bg_Command(string[] args) {
    screen.SetBg(args[0]);
    if(args.Length >= 2) {
      screen.SetFg(args[1]);
    }
  }

  void Fg_Command(string[] args) {
    screen.SetFg(args[0]);
  }

  void Character_Command(string[] args) {
    int index = 0;
    string[,] chars = new string[6, 2];
    chars[0, 0] = "None";
    chars[1, 0] = "None";
    chars[2, 0] = "None";
    chars[3, 0] = "None";
    chars[4, 0] = "None";
    chars[5, 0] = "None";
    for(int i = 1; i < args.Length; i += 3) {
      index = (int.Parse(args[i - 1])) - 1;
      chars[index, 0] = args[i];
      chars[index, 1] = args[i + 1];
    }	
    screen.SetCharacter(chars);
  }

  void Character_Reset_All_Command() {
    string[] args = {"1", "none", "0",
      "2", "none", "0",
      "3", "none", "0",
      "4", "none", "0",
      "5", "none", "0",
      "6", "none", "0",
    };
    Character_Command(args);
  }

  void Char_Sprite_Command(string[] param) {
		
    int convertedInt = 0;
    string[] chars = new string[6];
    chars[0] = "None";
    chars[1] = "None";
    chars[2] = "None";
    chars[3] = "None";
    chars[4] = "None";
    chars[5] = "None";
    for(int i = 0; i < param.Length; i++) {
      if(i % 2 != 0) {
        convertedInt = VSNController.GetInstance().charList.GetCharIdByParam(param[0]);
        chars[convertedInt] = param[i];
      }	
    }	
    screen.SetCharacterSprite(chars);
  }

  void Char_Base_Command(string[] param) {
    int convertedInt = 0;
    string[] chars = new string[6];
    chars[0] = "None";
    chars[1] = "None";
    chars[2] = "None";
    chars[3] = "None";
    chars[4] = "None";
    chars[5] = "None";
    for(int i = 0; i < param.Length; i++) {
      if(i % 2 != 0) {
        convertedInt = VSNController.GetInstance().charList.GetCharIdByParam(param[0]);
        chars[convertedInt] = param[i];
      } 
    } 
    screen.SetCharacterBaseSprite(chars);
  }

  void CharAnimate_Command(string[] param) {
    int characterId = VSNController.GetInstance().charList.GetCharIdByParam(param[0]);

    Debug.Log("character id to animate: " + characterId);
    Debug.Log("execute animation: " + param[1]);

    VSNController.GetInstance().charList.characters[characterId].Animate(param[1]);
  }


  void Say_Command(string[] args) {		
		
    VSNController.GetInstance().SetDialogState();
    if(args.Length > 1) {
      screen.SetDialog(args[0], args[1]);

      if((args[1])[0] != '(' && (args[1])[0] != '<')
        VSNController.GetInstance().charList.MakeCharTalk(args[0]);
    } else {
      screen.SetDialog(args[0]);

      if((args[0])[0] != '(' && (args[0])[0] != '<')
        VSNController.GetInstance().charList.MakeCharTalk(screen.GetTalkingCharName());
    }
  }

  void Say_Sfx_Command(string[] param) {
		
    string name;
    int interval;
    if(param.Length > 1) {
      name = param[0];
      interval = int.Parse(param[1]);
      screen.SetDialogSfx(name);
      screen.SetSfxInterval(interval);
      return;
    } else {
      bool result = int.TryParse(param[0], out interval);
      if(result) {
        screen.SetSfxInterval(interval);
        return;
      } else {
        name = param[0];
        screen.SetDialogSfx(name);
        return;
      }
    }
  }

  void Say_Pitch_Command(string[] param) {		
    for(int i = 0; i < param.Length; i += 2) {
			
      string name = param[i];
      float pitch = float.Parse(param[i + 1]);
//      AudioController.GetInstance().SetCharacterPitch(name, pitch);
    }
  }

  void Move_x_Command(string[] param) {
		
    int char_index = VSNController.GetInstance().charList.GetCharIdByParam(param[0]);
    float anim_time = float.Parse(param[1]);
    float destination_x = float.Parse(param[2]);
		
    screen.SetMovement_x(char_index, anim_time, destination_x);
  }

  void Move_y_Command(string[] param) {
		
    int char_index = VSNController.GetInstance().charList.GetCharIdByParam(param[0]);
    float anim_time = float.Parse(param[1]);
    float destination_y = float.Parse(param[2]);

    screen.SetMovement_y(char_index, anim_time, destination_y);
  }

  static void Mouth_Anim_Command(string[] param) {

    string animation_name = param[0];
    string[] sprite_names = new string[param.Length - 1];	
    int j = 1;
		
    for(int i = 0; i < sprite_names.Length; i++) {
      sprite_names[i] = param[j];
      j++;
    }
    CharacterAnimations.CreateMouthAnimation(animation_name);
    CharacterAnimations.AssignMouthAnimation(animation_name, sprite_names);
  }

  static void Eye_Blink_Anim_Command(string[] param) {

    string animation_name = param[0];
    string[] sprite_names = new string[param.Length - 1];
    int j = 1;
		
    for(int i = 0; i < sprite_names.Length; i++) {
      sprite_names[i] = param[j];
      j++;
    }
    CharacterAnimations.CreateEyeAnimation(animation_name);
    CharacterAnimations.AssignEyeAnimation(animation_name, sprite_names);
  }

  void AnimAlpha_Command(string[] param) {

    int char_index = VSNController.GetInstance().charList.GetCharIdByParam(param[0]);
    float alpha = float.Parse(param[1]);
    float time = float.Parse(param[2]);
		
    CharacterList.GetInstance().AnimateAlpha(char_index, time, alpha);
  }

  void Anim_Scale_Command(string[] param) {

    int char_index = VSNController.GetInstance().charList.GetCharIdByParam(param[0]);
    float scale = float.Parse(param[1]);
    float time = float.Parse(param[2]);

    screen.SetScale(char_index, time, scale);
  }

  void Question_Command(string[] text) {
		
    if(text.Length > 1) {
      screen.SetQuestion(text[0], text[1]);
			
      if((text[1])[0] != '(')
        VSNController.GetInstance().charList.MakeCharTalk(screen.GetTalkingCharName());
    } else {
      screen.SetQuestion(text[0]);

      if(text[0] == null)
        return;
			
      if((text[0])[0] != '(')
        VSNController.GetInstance().charList.MakeCharTalk(screen.GetTalkingCharName());
    }
  }


  void Choices_Command(string[] param) {
		
    string[] choiceText = new string[param.Length / 2];
    int[] choiceWaypoint = new int[param.Length / 2];
    string[] labelNames = new string[param.Length / 2];
    int tempIndex = 0;
		
    for(int i = 0; i < param.Length; i++) {
      if(i % 2 == 0) {
        choiceText[tempIndex] = param[i];
      } else {
        labelNames[tempIndex] = param[i];
        if(waypoints.ContainsKey(param[i])) {
          choiceWaypoint[tempIndex] = waypoints[param[i]];
        } else {
          //Debug.log("ERROR: " + "'" + param[i] + "'" + " is not a valid Waypoint. Choices_Command");
          choiceWaypoint[tempIndex] = 0;
        }
        tempIndex++;
      }
    }
    VSNController.GetInstance().SetLabelNames(labelNames);
    screen.SetChoices(choiceText, choiceWaypoint);
  }


  void PopQuestion_Command(){
    int currentPlace = Persistence.GetInstance().GetVariableValue("current_place");

//    if(!Persistence.debugMode){
      GameMapManager.GetInstance().placesArray[currentPlace-1].PopQuestion();
//    }
  }


  void CurrentIntroduction_Command() {
    string[] question = {Util.GetCharacterName(GlobalGameData.GetInstance().currentQuestion.character),  GlobalGameData.GetInstance().currentQuestion.introduction};
    Say_Command(question);
  }

  void CurrentQuestion_Command() {
    string[] question = {Util.GetCharacterName(GlobalGameData.GetInstance().currentQuestion.character), GlobalGameData.GetInstance().currentQuestion.text};
    Question_Command(question);
  }

  void CurrentChoices_Command() {
    Question question = GlobalGameData.GetInstance().currentQuestion;
    string[] choiceArgs = new string[6];
    List<int> argIds = new List<int>{0, 1, 2};

    for(int i=0; i<6; i+=2){
      int chosenPos = Random.Range(0, argIds.Count);
      int chosenId = argIds[chosenPos];
      argIds.RemoveAt(chosenPos);

      choiceArgs[i] = question.choices[chosenId];
      choiceArgs[i+1] = "item_" + (char)((int)'a'+chosenId);
    }

    Choices_Command(choiceArgs);
  }

  void LogAnswerEvent_Command(string[] param) {
    Debug.LogWarning("Sending Log Answer Event request!");

    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey("UserAnswersQuestion")
      .SetEventAttribute("GUESS", param[0])
      .SetEventAttribute("QUESTIONID", GlobalGameData.GetInstance().currentQuestion.id)
      .Send((response) => {
      if(!response.HasErrors) {
        Debug.LogWarning("Answer Event sent!");
        VSNController.GetInstance().SendAnswerConfirmation();
      } else {
        if(response.Errors.ContainsKey("answer_error") ){
          VSNController.GetInstance().SendAnswerDuplicateError();
        }else{
          VSNController.GetInstance().SendAnswerConnectionError();
        }
        Debug.LogError("Answer Event ERROR!");
      }
    });
  }

  void AddScriptData_Command(string[] param){
    Debug.LogWarning("Sending Add Script Data request!");

    Persistence.GetInstance().SetVariableValue(param[0], 1 );

    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey("AddScriptData")
      .SetEventAttribute("key", param[0])
      .SetEventAttribute("value", 1)
      .Send((response) => {
      if(!response.HasErrors) {
        Debug.LogWarning("Script Data sent!");
      } else {
        Debug.LogError("Script Data ERROR!");
      }
    });
  }
  




  void GotoNextElse() {
    int lineNumber = VSNScriptReader.GetInstance().GetNextElseLine();
    VSNScriptReader.GetInstance().GoToLine(lineNumber);
  }

  void GotoNextEndif() {
    int lineNumber = VSNScriptReader.GetInstance().GetNextEndifLine();
    VSNScriptReader.GetInstance().GoToLine(lineNumber + 1);
  }


  public void Goto_Command(string[] label) {
    if(waypoints.ContainsKey(label[0])) {
      int lineNumber = waypoints[label[0]];
//      //Debug.log("The label " + label[0] + " has value: "+lineNumber);
      VSNScriptReader.GetInstance().GoToLine(lineNumber);
    }
  }


  void Screenshake_Command(string[] param) {
    Debug.Log("Screenshake happening");
		
    if(param.Length != 1)
      Debug.Log("Incorrect use of Screenshake. Use Screenshake {x, where x is the intensity from 1 to 10");
    else {
      int amount = int.Parse(param[0]);
      if(amount >= 1 && amount <= 10)
        screen.Screenshake(amount);
    }
  }


  public void Fade_In_Command(string[] args) {
    fade.FadeIn(float.Parse(args[0]));
    Wait_Command(args);
  }

  public void Fade_Out_Command(string[] anim_time) {
    fade.FadeOut(float.Parse(anim_time[0]));
    Wait_Command(anim_time);
  }

  public void Fade_To_Command(string[] param) {
    fade.FadeTo(float.Parse(param[0]), float.Parse(param[1]));
    Wait_Command(param);
  }

  void Mirror_Command(string[] param) {
    int char_index = VSNController.GetInstance().charList.GetCharIdByParam(param[0]);
    screen.MirrorCharacter(char_index);
  }

  void Set_Var_Command(string[] param) {
    string key = param[0];
    int value = int.Parse(param[1]);
		
    Persistence.GetInstance().SetVariableValue(key, value);
  }

  void Set_Var_Bonus_Command(string[] param) {
    string key = param[0];

    if(GlobalGameData.GetInstance().currentBonusPeriod != null) {
      Debug.LogWarning("PROMOÇÃO!");
      Persistence.GetInstance().SetVariableValue(key, 1);
    } else {
      Debug.LogWarning("SEM PROMOÇÃO!");
      Persistence.GetInstance().SetVariableValue(key, 0);
    }
  }

  void Set_Number_Victories(string[] param) {
    string key = param[0];

    Persistence.GetInstance().SetVariableValue(key, Persistence.matchVictories);
  }

  void If_Command(string[] param) {
    string varName = param[0];
    string operatorVal = param[1];
    int value = int.Parse(param[2]);
    string[] waypointName = new string[1];
    if(param.Length == 4)
      waypointName[0] = param[3];
    int varValue = Persistence.GetInstance().GetVariableValue(varName);
		
    OperationFactory op = new OperationFactory();
    Debug.Log("If " + varValue + " " + operatorVal + " " + value);
    if(op.Run(varValue, operatorVal, value) == false) {
      if(param.Length == 4)
        Goto_Command(waypointName);
      else
        GotoNextElse();
    }
  }

  void Else_Command() {
    GotoNextEndif();
  }


  void SayBonus_Command(string[] param) {
    string[] say_param = {"", ""};
    GlobalGameData gameData = GlobalGameData.GetInstance();

    if(int.Parse(param[0]) == 1) {
      say_param[1] = "<color=#FFCB06FF>PROMOÇÃO: " + gameData.currentBonusPeriod.title + "</color>" + "\n" +
        "Do dia <color=#FFCB06FF>" + gameData.currentBonusPeriod.startDate +
        "</color> até <color=#FFCB06FF>" + gameData.currentBonusPeriod.endDate +
        "</color> todas as perguntas estão valendo <color=#FFCB06FF>" + gameData.currentBonusPeriod.bonus +
        "%</color> de pontos extras. Aproveite!";
    } else if(int.Parse(param[0]) == 2) {
      say_param[1] = GlobalGameData.GetInstance().currentBonusPeriod.description;
    }
    Say_Command(say_param);
  }

  void EarnPoints_Command() {
    int pointsToEarn = Util.PointsToEarnByTheme(GlobalGameData.GetInstance().currentQuestion.theme);
    int bonusPoints;
    Debug.Log("Base points to get: " + pointsToEarn);

    if(GlobalGameData.GetInstance().currentBonusPeriod != null){
      bonusPoints = (pointsToEarn * GlobalGameData.GetInstance().currentBonusPeriod.bonus / 100);
      Debug.Log("Bonus points to get: " + bonusPoints);
      pointsToEarn += bonusPoints;
    }

    CheckIfPromoted(pointsToEarn);
    GameSparksManager.GetInstance().PlayerEarnPoints(pointsToEarn);

    string[] say_param = { "", "PARABÉNS! Você ganhou " + pointsToEarn.ToString() + " pontos!" };
    Say_Command(say_param);
  }

  void CheckIfPromoted(int pointsToEarn) {
    int currentPosition = Util.GetPositionIdByPoints(GlobalGameData.GetInstance().userScore);
    int newPosition = Util.GetPositionIdByPoints(GlobalGameData.GetInstance().userScore + pointsToEarn);

    if(currentPosition != newPosition) {
      // promote character
      Persistence.GetInstance().SetVariableValue("promocao", 1);
      Persistence.GetInstance().SetVariableValue("player_position", newPosition);
      CheckToGetAchievement(newPosition);
    }
  }

  void CheckToGetAchievement(int newPosition) {
    switch(Util.GetPositionNameById(newPosition)) {
      case "Analista Júnior":
        AchievementsManager.GetInstance().SendGetAchievementRequest("futuroPromisor");
        break;
      case "Gerente":
        AchievementsManager.GetInstance().SendGetAchievementRequest("liderancaNata");
        break;
      case "Presidente":
        AchievementsManager.GetInstance().SendGetAchievementRequest("unimedFortalezaNoTopo");
        break;
      case "Presidente 2":
        AchievementsManager.GetInstance().SendGetAchievementRequest("umPoucoDoisBom");
        break;
      case "Presidente 3":
        AchievementsManager.GetInstance().SendGetAchievementRequest("fazendoAsMalas");
        break;
      case "Presidente 4":
        AchievementsManager.GetInstance().SendGetAchievementRequest("venceuNaVida");
        break;
    }
  }


  void Add_Var_Command(string[] param) {
    string key = param[0];
    int value = int.Parse(param[1]);
		
    Persistence.GetInstance().SetVariableValue(key, Persistence.GetInstance().GetVariableValue(key) + value);
  }

  void Checkpoint_Command(string[] param) {
    int chapter = VSNController.GetInstance().chapterId;
    int checkpoint = int.Parse(param[0]);
    string msg;

    Resources.UnloadUnusedAssets();

    // sound effect
//    AudioController.GetInstance().PlayConfirmSound();

    // informative message
    if(Persistence.ReachCheckpoint(chapter, checkpoint)) {
      msg = "<color=#ac5100ff>[</color><color=#aa1100ff>O SEU PROGRESSO ESTÁ SALVO!</color><color=#ac5100ff> Você pode continuar o jogo a partir deste ponto através do Menu Inicial.]</color>";
    } else {
      msg = "<color=#ac5100ff>[</color><color=#aa1100ff>PONTO DE RETORNO ALCANÇADO!</color><color=#ac5100ff> Você pode continuar o jogo a partir deste ponto através do Menu Inicial.]</color>";
    }
    string[] info_msg = { "", msg };
    Say_Command(info_msg);
  }

  void To_Next_Chapter_Command() {
    VSNController.GetInstance().AdvanceChapter();
    VSNController.GetInstance().WaitSeconds(3f);
  }

  public static string GetStaticCommand(string line) {
    int start = line.IndexOf("@");
    int end = line.IndexOf(" ", start + 1);
    if(end <= 0)
      end = line.Length - 1;
    else
      end--;
    string command = line.Substring(start + 1, end - start);
    return command.ToLower();
  }

  public string GetCommand(string line) {

    line = Regex.Match(line, "[a-zA-Z_/]+").Value;
    Debug.LogWarning("Command cleaned up: " + line);
    return line.ToLower();
  }

  public static string[] GetParams(string line) {

    List<string> arrayStrings = new List<string>();

    Match match = Regex.Match(line, @"({[^{]*)+");

    Debug.Log("Command: " + line);

    foreach(Capture c in match.Groups[1].Captures) {
      string value = c.Value;

      value = Regex.Replace(value, "^{", "");
      value = value.Trim();

      Debug.Log("Arg: " + value);

      arrayStrings.Add(value);
    }

    return arrayStrings.ToArray();
  }
}
