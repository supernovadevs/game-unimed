
/// tutorial de leaderboard
if {tutorial_4 {== {0
  wait {1
  say {Este é o <color=#FFCB06FF>Ranking Geral</color>, onde você pode ver sua colocação em relação aos demais jogadores. A pontuação considerada é a soma de todos os pontos que você conseguiu respondendo as perguntas.
  
  show_tutorial_icon {6 {true
  wait {2
  say {Clicando neste botão você visualiza sua colocação no Ranking Geral.
  show_tutorial_icon {6 {false
  wait {0.5
  
  show_tutorial_icon {7 {true
  wait {2
  say {E clicando neste botão você visualiza o Ranking Geral completo, começando pelos primeiros lugares do jogo.
  show_tutorial_icon {7 {false
  wait {0.5
  
  say {Você se considera uma pessoa competitiva? Será que consegue ficar entre os primeiros colocados?
  
  add_script_data {tutorial_4
endif

end_script
