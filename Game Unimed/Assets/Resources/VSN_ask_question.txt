set_var {promocao {0

if {current_question_has_intro {== {1
  current_introduction
endif
start_count_time
current_question
current_choices


waypoint {item_a
log_answer_event {a
show_loading_icon {true
wait_answer_confirm {erro_envio {erro_duplicata
pop_question
show_loading_icon {false
if {current_answer {== {0
  goto {resp_certa
endif
goto {resp_errada


waypoint {item_b
log_answer_event {b
show_loading_icon {true
wait_answer_confirm {erro_envio {erro_duplicata
pop_question
show_loading_icon {false
if {current_answer {== {1
  goto {resp_certa
endif
goto {resp_errada


waypoint {item_c
log_answer_event {c
show_loading_icon {true
wait_answer_confirm {erro_envio {erro_duplicata
pop_question
show_loading_icon {false
if {current_answer {== {2
  goto {resp_certa
endif
goto {resp_errada


waypoint {resp_errada

character {2 {resposta_errada {0
anim_alpha {2 {0 {0
anim_alpha {2 {1 {1

sfx {Answer Wrong
char_animate {1 {char-sad
wait {2
goto {fim_script


waypoint {resp_certa
check_quick_answer

character {2 {resposta_correta {0
anim_alpha {2 {0 {0
anim_alpha {2 {1 {1

sfx {Answer Right
char_animate {1 {char-happy
wait {2
earn_points

if {promocao {== {1
  anim_alpha {2 {0 {1
  wait {1
  goto_script {VSN_promotion
endif

goto {fim_script






waypoint {erro_envio
show_loading_icon {false
say { {Ocorreu um erro no envio da sua resposta para o servidor! Por favor verifique sua conexão com a internet e responda a pergunta novamente.
goto_script {VSN_end_dialog




waypoint {erro_duplicata
show_loading_icon {false
pop_question
say { {Ocorreu um erro: essa pergunta já foi respondida. Sua resposta já foi computada e sua pontuação foi creditada corretamente.
say { {Caso nunca tenha respondido esta pergunta, por favor comunique os administradores do jogo.
goto_script {VSN_end_dialog




waypoint {fim_script
anim_alpha {2 {0 {1
wait {1

goto_script {VSN_end_dialog
