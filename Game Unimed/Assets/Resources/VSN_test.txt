set_var {promocao {0

start_count_time
question { {pergunta
choices {certa {resp_certa {errada {resp_errada {errada {resp_errada

waypoint {item_a
log_answer_event {a
if {current_answer {== {0
  goto {resp_certa
endif
goto {resp_errada

waypoint {item_b
log_answer_event {b
if {current_answer {== {1
  goto {resp_certa
endif
goto {resp_errada

waypoint {item_c
log_answer_event {c
if {current_answer {== {2
  goto {resp_certa
endif
goto {resp_errada


waypoint {resp_errada

character {2 {resposta_errada {0
anim_alpha {2 {0 {0
anim_alpha {2 {1 {1

sfx {Answer Wrong
char_animate {1 {char-sad
wait {2
goto {fim_script


waypoint {resp_certa
check_quick_answer

character {2 {resposta_correta {0
anim_alpha {2 {0 {0
anim_alpha {2 {1 {1

sfx {Answer Right
char_animate {1 {char-happy
wait {2
earn_points

if {promocao {== {1
  anim_alpha {2 {0 {1
  wait {1
  goto_script {VSN_promotion
endif

goto {fim_script


waypoint {fim_script
anim_alpha {2 {0 {1
wait {1

goto_script {VSN_end_dialog
