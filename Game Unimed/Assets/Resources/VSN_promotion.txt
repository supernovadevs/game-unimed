set_var {promocao {0

char_animate {1 {char-applauses
wait {1
show_victory_screen {true
sfx {Promotion
wait {2

say { {Hoje é um dia de comemoração! Você foi promovido!

if {player_position {== {1
  say { {Você juntou muitos pontos e atingiu o cargo de <color=#FFCB06FF>Estagiário</color>.
  say {Ao atingir o cargo de <color=#FFCB06FF>Analista Júnior</color> você tem direito a receber um brinde físico. Continue conseguindo mais pontos para subir de cargo!
  goto {receber_brinde
endif
if {player_position {== {4
  say { {Você atingiu o cargo de <color=#FFCB06FF>Analista Júnior</color>. Entre em contato com a Comunicação para retirar o seu brinde.
  goto {receber_brinde
endif
if {player_position {== {11
  say { {Parabéns, você atingiu o cargo de <color=#FFCB06FF>Presidente</color> e ganhou o 1º tíquete virtual. A partir de agora, você passa a concorrer ao sorteio de uma incrível viagem para a Disney.
  say {Continue jogando e conquistando mais tíquetes para o sorteio!
  goto {receber_brinde
endif
if {player_position {== {12
  say { {PARABÉNS! Você atingiu o cargo de <color=#FFCB06FF>Presidente 2</color>!
  say { {Você receberá um tíquete extra para participar do sorteio, e terá agora o dobro de chances!
  goto {receber_brinde
endif
if {player_position {== {13
  say { {PARABÉNS! Você atingiu o cargo de <color=#FFCB06FF>Presidente 3</color>!
  say { {Você receberá um tíquete extra para participar do sorteio, e terá agora o triplo de chances!
  goto {receber_brinde
endif
if {player_position {== {14
  say { {PARABÉNS! Você atingiu o cargo de <color=#FFCB06FF>Presidente 4</color>!
  say { {Você receberá um tíquete extra para participar do sorteio, e terá agora quatro vezes mais chances!
  goto {receber_brinde
endif
goto {fim_script


waypoint {receber_brinde


waypoint {fim_script
show_victory_screen {false

goto_script {VSN_end_dialog
