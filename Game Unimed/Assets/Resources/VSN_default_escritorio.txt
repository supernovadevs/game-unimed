fade_out {1
bg {escritorio
character {1 {arthur {5.1
fade_in {1

if {conhece_arthur {== {0
  char_animate {1 {char-waving
  wait {2
  say {Arthur {Oi, bom dia! Eu sou Arthur, sou um gestor muito responsável e compromissado com o Planejamento Estratégico da cooperativa. Faço parte do time há oito anos.
  say {Utilizo sempre o Planejamento como direcionador para ajudar minha equipe a alcançar os melhores resultados.
  say {Muita gente me acha muito sério, mas não se preocupe que eu não sou tão rígido quanto acham.
  say {Que tal me ajudar respondendo a umas perguntinhas? Eu lhe aviso quando tiver algo em mente.
  add_script_data {conhece_arthur
  goto {fim_script
endif

char_animate {1 {char-waving
wait {2
say {Arthur {Olá, tudo bem?
say {Está tudo sob controle por aqui. Obrigado por perguntar.

waypoint {fim_script
fade_out {1
bg {none {none
character_reset_all
fade_in {1
end_script
