
/// tutorial introdutorio
if {tutorial_1 {== {0
  character {1 {jose_carlos {-2.4 {2 {arthur {-4.8 {3 {sofia {0 {4 {roberta {2.4 {5 {ana {4.8
  anim_alpha {1 {0 {0
  anim_alpha {2 {0 {0
  anim_alpha {3 {0 {0
  anim_alpha {4 {0 {0
  anim_alpha {5 {0 {0
  
  set_question_icons_visible {false
  fade_in {1
  
  say { {Olá! Seja bem-vindo ao Jornada - O Jogo da Excelência! (Se você já possuía uma conta e está vendo esta mensagem, favor contactar os organizadores do jogo.)
  say {Este é um jogo onde você irá testar seus conhecimentos, ficar por dentro de tudo sobre a Unimed Fortaleza, ganhar brindes, e ainda concorrer a uma incrível viagem!
  say {Seu personagem é um Aprendiz recém-contratado que pode ser promovido...
  
  anim_alpha {1 {1 {1
  anim_alpha {2 {1 {1
  anim_alpha {3 {1 {1
  anim_alpha {4 {1 {1
  anim_alpha {5 {1 {1
  wait {1
  
  say {...ajudando os demais personagens que você encontrará, que são seus colegas no jogo .
  
  char_animate {1 {char-waving
  char_animate {2 {char-waving
  char_animate {3 {char-waving
  char_animate {4 {char-waving
  char_animate {5 {char-waving
  wait {1.8
  
  anim_alpha {1 {0 {1
  anim_alpha {2 {0 {1
  anim_alpha {3 {0 {1
  anim_alpha {4 {0 {1
  anim_alpha {5 {0 {1
  wait {1
  character_reset_all
  
  say {Para isto você deve responder as perguntas que os personagens irão lhe fazer.
  
  set_places_outline_blink {true
  wait {2
  
  say {Estes são os lugares onde você poderá encontrá-los.
  
  set_places_outline_blink {false
  
  set_question_icons_visible {true
  wait {1
  
  say {Perceba que alguns lugares estão marcados com exclamações: elas indicam onde seus colegas estão precisando de ajuda agora. Clique neles para responder suas perguntas e ganhar pontos.
  
  add_script_data {tutorial_1
else
  fade_in {1
endif


/// Mostrar período bônus, se houver algum
if {has_bonus_period {== {1
  if {tutorial_5 {== {0
    say {Em alguns períodos do ano ocorrem eventos promocionais, nos quais as perguntas valem mais pontos do que o normal por alguns dias.
    say {Aproveite para responder muitas perguntas e ganhar mais pontos!
    add_script_data {tutorial_5
  endif
  say_bonus {1
  say_bonus {2
endif


end_script
