fade_out {1
bg {praca
character {1 {sofia {5.1
fade_in {1

if {conhece_sofia {== {0
  char_animate {1 {char-waving
  wait {2
  say {Sofia {Olá, bom dia! Você deve ser o novo colaborador, não é? Meu nome é Sofia.
  say {Eu trabalho há quase 13 anos na equipe da ASERH. Entrei na cooperativa muito jovem como aprendiz e depois de muito trabalho e esforço cheguei ao meu cargo atual de gerente de RH.
  say {Fico muito feliz em conhecer você! Eu sempre faço questão de conhecer os novos colaboradores. Gostaria de poder conhecer a todos pessoalmente!
  say {Está tudo bem por aqui. Obrigada por vir me cumprimentar.
  add_script_data {conhece_sofia
  goto {fim_script
endif

char_animate {1 {char-waving
wait {2
say {Sofia {Olá, tudo bem?
say {Está tudo sob controle por aqui. Obrigada por perguntar.


waypoint {fim_script
fade_out {1
bg {none {none
character_reset_all
fade_in {1
end_script
