﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tut_Blinking : MonoBehaviour {

  public float blinkingTime;
  public float minAlpha =0f;
  public float maxAlpha =1f;

  void Start(){
    Initialize();
  }

  void Initialize(){
    StartCoroutine(SmoothBlink());
  }

  IEnumerator SmoothBlink(){
    SetAlpha(minAlpha);
    while(true) {
      yield return SmoothFade(minAlpha, maxAlpha, blinkingTime / 2f);
      yield return SmoothFade(maxAlpha, minAlpha, blinkingTime / 2f);
    }
  }


  IEnumerator SmoothFade(float initialAlpha, float finalAlpha, float seconds) {
    float t = 0f;
    while (t <= 1f) {
      t += Time.deltaTime/seconds;
      SetAlpha( Mathf.SmoothStep(initialAlpha, finalAlpha, t) );
      yield return null;
    }
  }


  void SetAlpha(float alpha){
    Color c = GetComponent<Image>().color;
    c.a = alpha;
    GetComponent<Image>().color = c;
  }
}
